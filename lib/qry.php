<?php

/**
 * Created by PhpStorm.
 * User: Роман
 * Date: 12.03.2019
 * Time: 12:57
 */
class qry
{

    public function __construct()
    {

    }

    /**
     * @param $sql
     * @param int $localDb
     * @param bool $assoc
     * @return array|bool|PDOStatement
     */
    public static function queryGet($sql, $localDb = 1, $assoc=true)
    {
        $result = false;

        switch ($localDb) {
            case 2:
                $db = DB::getConnection2();
                break;
            case 3:
                $db = DB::getConnection3();
                break;
            default:
                $db = DB::getConnection();
                break;
        }

        if($db !== null) {
            $result = $db->prepare('SET DATEFORMAT dmy  ' . $sql);
            try {
                $result->execute();
            } catch (PDOException $Exception) {
                self::logB($sql);
                self::logB($Exception->getCode() . ":" . $Exception->getMessage());
                var_dump($Exception->getCode() . ":" . $Exception->getMessage());
            }
         $result = ($assoc) ? $result->fetchAll(PDO::FETCH_ASSOC) : $result->fetchAll();
        }
        return $result;
    }


    /**
     * @param $sql
     * @param int $localDb
     * @param bool $returnID
     * @return bool|string
     */
    public static function queryExec($sql, $localDb = 1, $returnID = false, $count=false)
    {
        switch ($localDb) {
            case 2:
                $db = DB::getConnection2();
                break;
            case 3:
                $db = DB::getConnection3();
                break;
            default:
                $db = DB::getConnection();
                break;
        }
        
        $result = $db->prepare('SET DATEFORMAT dmy  ' . $sql);
        try {
            if ($res = $result->execute()) {
                if ($returnID) {
                    return $db->lastInsertId();
                }
                if($count){
                    return $result->rowCount();
                }
                return true;
            }
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            //var_dump($Exception->getCode() . ":" . $Exception->getMessage());
            return false;

        }
        return false;
    }
    public static function queryExecPrepare($sql, $array, $localDb = 1, $returnID = false)
    {
        $db = DB::getConnection();
        $result = $db->prepare('SET DATEFORMAT dmy  ' . $sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        try {
            if ($result->execute($array)) {
                unset($db);
                unset($result);
                return true;
            }
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            //var_dump($Exception->getCode() . ":" . $Exception->getMessage());
            return false;

        }
        return false;
    }
    /**
     * @param $dbase
     * @param $rowName
     * @param $value
     * @return bool
     */
    public static function queryInsert($dbase, $rowName, $value)
    {
        $db = DB::getConnection();
        $sql = 'SET DATEFORMAT dmy INSERT INTO ' . $dbase . ' (' . $rowName . ') VALUES(' . $value . ')';
        $result = $db->prepare($sql);
        try {
            ;
            if ($result->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }
        return false;
    }

    /**
     * @param $sql
     * @return array|bool|PDOStatement
     */
    public static function queryExist($sql)
    {
        $db = DB::getConnection();
        $result = $db->prepare('SET DATEFORMAT dmy  ' . $sql);
        try {
            $result->execute();
            if ($result->fetch(PDO::FETCH_NUM)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }
        $result = $result->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * обработка $_post $_get запросов
     * @return mixed
     */
    public static function rout()
    {
        return (!empty($_POST)) ? $_POST : $_GET;
    }

    public static function logB($val)
    {
        $today = date("d-m-y");
        $now = date("d-m-y H:i:s");
        $p = './content/app-assets/files/Log/SQLLog/' . $today . '/';
        classFilesOp::mkDirPS($p);
        file_put_contents($p . 'log.txt', $now . PHP_EOL . '| ' . $_SESSION['dickrname'] . ' | ' . ' | ' . $_SESSION['pageN'] . ' |' . PHP_EOL . $val . PHP_EOL, FILE_APPEND);
    }

    public static function insertJSON($rout)
    {//API BD
        $json = json_decode($rout['data'], true);

        //print_r($json);
        //-Готовим запрос-
        //$json.nameDB
        //$json.nameTable
        //$json.nameColume
        //foreach ($json as $k=>$value) {}
        //echo $rout['nameColume'];
        $name = '';
        switch ($rout['cmd']) {
            case "u"://Формирование нормы расходов
                foreach ($json['nameColume'] as $k => $value) {
                    $name .= $k . "='" . $value . "',";
                }
                $where = $json['where'];
                $name = substr($name, 0, -1);
                $sql = "UPDATE  " . $json['nameDB'] . ".." . $json['nameTable'] . " SET $name where $where \n";
                if (qry::queryExec($sql)) {
                    echo 'ok';
                } else {
                    echo 'error';
                };
                exit;
                break;
            case "i":
                $val = '';
                foreach ($json['nameColume'] as $k => $value) {
                    $name .= $k . ',';
                    $val .= "'" . $value . "',";
                }
                $name = substr($name, 0, -1);
                $val = substr($val, 0, -1);
                $sql = "INSERT INTO " . $json['nameDB'] . ".." . $json['nameTable'] . " (" . $name . ") VALUES(" . $val . ")";
                if (qry::queryExec($sql)) {
                    echo 'ok';
                } else {
                    echo 'error';
                };
                exit;
                break;
            case "iu":
                $val = '';
                $sel = '';
                $upd = '';
                $sql2 = '';
                foreach ($json['nameColume'] as $k => $value) {
                    $name .= $k . ',';
                    $val .= "'" . $value . "',";
                    $sel .= $k . "='" . $value . "' and ";
                    $upd .= $k . "='" . $value . "',";
                }

                $sel = substr($sel, 0, -5);
                $name = substr($name, 0, -1);
                $val = substr($val, 0, -1);
                $upd = substr($upd, 0, -1);

                $sql2 .= "
                    IF NOT EXISTS(SELECT TOP 1 1 FROM " . $json['nameDB'] . ".." . $json['nameTable'] . " WHERE 
                        $sel) BEGIN
                        INSERT INTO " . $json['nameDB'] . ".." . $json['nameTable'] . " (" . $name . ") VALUES(" . $val . ") \n
                    END ELSE BEGIN
                        SELECT * FROM documents..documentTheRateOf WHERE $sel
                        UPDATE  " . $json['nameDB'] . ".." . $json['nameTable'] . " SET $upd \n
                        END
                ";
                echo $sql2;
                exit;
                break;
            case "d":
                $sql = '';
                echo $sql;
                exit;
                break;
        }
    }

}
