<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class auth {
    function __construct(){
        global $pg;
        $method = $pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Установить авторизацию
    static function setAuth($idUser=false){
        global $pg;
        $where = ($idUser) ? "users.id=$idUser" : "([login]='{$pg['login']}' OR [email]='{$pg['login']}') AND password='{$pg['password']}'";
        $sql = "
SELECT
       users.[id]
      ,users.[surName]
      ,users.[firstName]
      ,users.[lastName]
      ,users.[email]
      ,users.[phone]
      ,users.[access]
      ,users.[appDefault]
      ,users.companyID
      ,company.[INN]
      ,company.[KPP]
      ,company.[name] as company
      ,company.[head] as companyHead
      ,company.[lock]
FROM [globalAuth].[dbo].[users]
LEFT JOIN [globalAuth].[dbo].[company] ON company.id=users.companyID
WHERE $where
AND [access] > 0
AND [confirm] = 0
;";
        $user = qry::queryGet($sql)[0];

if($user) {
    if (!empty($pg['IMEI'])) {
        $app = ($user['appDefault']) ? "'{$user['appDefault']}'" : "NULL";
        $lat = ($pg['lat']) ? "'{$pg['lat']}'" : "NULL";
        $long = ($pg['long']) ? "'{$pg['long']}'" : "NULL";
        $sql = "
        UPDATE [globalAuth].[dbo].[phoneList]
        SET [userID] = '{$user['id']}',
        [lat] = $lat,
        [long] = $long,
        [dateTime] = '{$pg['dateTime']}',
        [timeZone] = '{$pg['timeZone']}',
        [app] = $app,
        [dateTimeS] = GETDATE()
        WHERE IMEI='{$pg['IMEI']}';
        if @@ROWCOUNT = 0
        INSERT INTO [globalAuth].[dbo].[phoneList] (
       [userID]
      ,[IMEI]
      ,[lat]
      ,[long]
      ,[dateTime]
      ,[timeZone]
      ,[app]
        ) VALUES (
        '{$user['id']}',
        '{$pg['IMEI']}',
        $lat,
        $long,
        '{$pg['dateTime']}',
        '{$pg['timeZone']}',
        $app
        );
        ";
        qry::queryExec($sql);
    }
$access = qry::queryGet("
SELECT
name,
title,
icon,
mobile
FROM [globalAuth].[dbo].[project]
WHERE id IN (
     SELECT idProject FROM [globalAuth].[dbo].[access]
     WHERE companyID='{$user['companyID']}'
)
");
}else{$access = null;}

        global $salt;
        $hash = ($user) ? explode("$",crypt(json_encode($user,JSON_UNESCAPED_UNICODE).json_encode($access,JSON_UNESCAPED_UNICODE), $salt))[4] : null;
        $fHash = $hash?"'$hash'":"NULL";
        $userID = $idUser?:$user['id'];
        if(!qry::queryExec("UPDATE [globalAuth].[dbo].[users] SET [hash]=$fHash WHERE [id]='$userID'"))return array('auth'=>false);
        return array(
            'auth' => !!$user,
            'info' => $user,
            'hash' => $hash,
            'access' => $access
        );
    }

    //Проверка авторизации
    static function testAuth(){
        auth::debit();
//        global $pg;
//        $ssdir = "./session/";
//        if(isset($pg['HCSSID']) && file_exists("$ssdir{$pg['HCSSID']}.json")){
//            $_SESSION['auth'] = json_decode(file_get_contents("$ssdir{$pg['HCSSID']}.json"), true);
//            touch("$ssdir{$pg['HCSSID']}.json");
//        }
//        $array = array('auth'=>qry::queryExist("SELECT hash FROM [globalAuth].[dbo].[users] WHERE hash='{$pg['hash']}'"));
//        if($array['auth'] AND (!isset($_SESSION['auth']) OR $_SESSION['auth']['hash']!=$pg['hash'])){
//            $_SESSION['auth'] = auth::getAuth();
//            $array['HCSSID'] = md5(uniqid());
//            auth::setSession($array['HCSSID']);
//        }
//        elseif(!$array['auth'] AND isset($_SESSION['auth'])){
//            $array=$_SESSION['auth']=auth::setAuth($_SESSION['auth']['info']['id']);
//            auth::setSession($pg['HCSSID']);
//        }
//        auth::clearSession();
//        return $array;

        global $pg;
        global $salt;
        $array = array('auth'=>false);
        $ssdir = "./session/";
        if (!file_exists($ssdir))mkdir($ssdir, null, true);
        if($pg['hash']){
            $corrInfo = true;
            $auth = false;
            if ($pg['auth']) {
                $auth = json_decode($pg['auth'], true);
                $hash = explode("$", crypt(json_encode($auth['info'], JSON_UNESCAPED_UNICODE) . json_encode($auth['access'], JSON_UNESCAPED_UNICODE), $salt))[4];
                $corrInfo = $auth['hash'] == $hash && $pg['hash'] == $hash;
            }
            if($corrInfo) {
                $ssid = urlencode($pg['hash']);
                $existDB = qry::queryExist("SELECT hash FROM [globalAuth].[dbo].[users] WHERE hash='{$pg['hash']}' AND [access]>0");
                if (file_exists("{$ssdir}{$ssid}.json")) {
                    $_SESSION['auth'] = json_decode(file_get_contents("{$ssdir}{$ssid}.json"), true);
                    if ($existDB) {
                        touch("{$ssdir}{$ssid}.json");
                        $array['auth'] = true;
                    } else {
                        $_SESSION['auth'] = auth::setAuth($_SESSION['auth']['info']['id']);
                        if($_SESSION['auth']['auth']) {
                            $ssid = urlencode($_SESSION['auth']['hash']);
                            file_put_contents("{$ssdir}{$ssid}.json", json_encode($_SESSION['auth'], JSON_UNESCAPED_UNICODE));
                        }
                        $array = $_SESSION['auth'];
                    }
                } elseif ($auth && $existDB){
                    $_SESSION['auth'] = $auth;
                    file_put_contents("{$ssdir}{$ssid}.json", $pg['auth']);
                    $array['auth'] = true;
                }
            }
        }


        //очистка файлов кастомной сессии
        $filename = "time-clear.txt";
        $lastDate = (file_exists($filename)) ? file_get_contents($filename) : time() - 86500;
        $diff = (time() - $lastDate) / 86400;
        if ($diff > 1) {
            file_put_contents($filename, time());
            if (file_exists($ssdir)) {
                foreach(scandir($ssdir) as $value){if($value != '.' && $value != '..'){
                    $diff = (time() - filemtime($ssdir . $value)) / 3600;
                    if ($diff > 5) unlink($ssdir . $value);
                }}
            }
        }

        return $array;
    }

    //Установить ограничение по правам на функцию
    static function giveAccess($access){
        global $pg;
        global $devKey;
        if($pg['hash']!=$devKey && $_SESSION['auth']['info']['access'] < $access){
            header("HTTP/1.1 403 Forbidden");
            exit;
        }
    }

    //лог установки мобильного приложения
    public function appInstall(){
        global $pg;
        $sql = "UPDATE [globalAuth].[dbo].[phoneList] SET [app]='{$pg['app']}' WHERE IMEI='{$pg['imei']}';";
        return qry::queryExec($sql);
    }

    //Авторизация по LDAP для АСУДД
    public function asuddAuth(){
        global $pg;
        $idUser = md5("asudd{$pg['login']}asudd")==$pg['hash'] ? qry::queryGet("SELECT id FROM [globalAuth].[dbo].[users] WHERE login='{$pg['login']}'")[0]['id'] : null;
        return auth::setAuth($idUser);
    }

    //регистрация через мобильный маркет
    public function regUser(){
        global $pg;
        $data = json_decode($pg['data']);
        if(qry::queryExist("SELECT login FROM [globalAuth].[dbo].[users] WHERE login='{$data->login}';"))return array('set'=>2);
        if(qry::queryExist("SELECT email FROM [globalAuth].[dbo].[users] WHERE email='{$data->email}';"))return array('set'=>3);
        if(qry::queryExist("
            SELECT id FROM [globalAuth].[dbo].[users]
            WHERE [companyID]='{$data->companyID}'
            AND REPLACE([surName],'ё','е')=REPLACE('{$data->surName}','ё','е')
            AND REPLACE([firstName],'ё','е')=REPLACE('{$data->firstName}','ё','е')
            AND REPLACE([lastName],'ё','е')=REPLACE('{$data->lastName}','ё','е')
        ;"))return array('set'=>4);
            $sql = "
        INSERT INTO [globalAuth].[dbo].[users](
       [login]
      ,[password]
      ,[surName]
      ,[firstName]
      ,[lastName]
      ,[email]
      ,[phone]
      ,[companyID]
      ,[confirm]
        )VALUES(
        '{$data->login}',
        '{$data->password}',
        '{$data->surName}',
        '{$data->firstName}',
        '{$data->lastName}',
        '{$data->email}',
        '{$data->phone}',
        '{$data->companyID}',
        '1'
        );
        ";
        $idUser = qry::queryExec($sql,1,true);
        $res = $idUser ? 1 : 0;
        return array('set'=>$res, 'idUser'=>$idUser);
    }

    //получить фото пользователя по id
    public function getUserPhoto(){
        global $pg;
        $file = "./user-photo/user_{$pg['idUser']}";
        if(file_exists("$file.jpg"))$exp = "jpg";
        elseif(file_exists("$file.jpeg"))$exp = "jpeg";
        elseif(file_exists("$file.png"))$exp = "png";
        else{
            $file = "./user-photo/no-photo";
            $exp = "jpg";
        }
        if($exp==='png')header("Content-type: image/png");
        else header("Content-type: image/jpeg");
        if($pg['thumb'] && file_exists("{$file}_thumb_{$pg['thumb']}.$exp")){
            $file = "{$file}_thumb_{$pg['thumb']}";
        }
        $name = end(explode("/",$file));
        header("Content-Disposition: filename=\"$name.$exp\"");
        if(ob_get_level())ob_end_clean();
        readfile("$file.$exp");
    }

    //Получить статус регистрации в мобильном маркете
    public function getStatusReg(){
        global $pg;
        $sql = "SELECT confirm FROM [globalAuth].[dbo].[users] WHERE id='{$pg['idUser']}'";
        $user = qry::queryGet($sql);

        if(count($user)==0)$statusReg = 'error';
        elseif($user[0]['confirm']==0)$statusReg = 'success';
        else $statusReg = 'waiting';

        return array('statusReg'=>$statusReg);
    }

    //Получить список компаний
    public function getCompany(){
        return company::getCompany();
    }

    //биллинг списание абонентской платы
    static function debit(){
        $filename = "lock-debit.txt";
        $f = fopen($filename,'a');
        if (flock($f, LOCK_EX | LOCK_NB)) {

            $dateNow = date("Y-m-d");
            $company = qry::queryGet("SELECT [id],[balance],[tariff],[dateDebit],[lock] FROM [globalAuth].[dbo].[company]");
            $sql = "";
            foreach ($company as $key => $val) {
                if ($dateNow > $val['dateDebit']) {
                    $sumDay = (strtotime($dateNow) - strtotime($val['dateDebit'])) / 86400;
                    if ($sumDay == 1 || $val['lock'] == '1') {
                        if ($val['balance'] >= $val['tariff']) { //списание за день
                            $sql .= "UPDATE [globalAuth].[dbo].[company] SET [balance]=[balance]-{$val['tariff']},[dateDebit]='$dateNow',[lock]=0 WHERE [id]='{$val['id']}';";
                            if ($val['tariff'] > 0) {//логирую
                                $sql .= "INSERT INTO [globalAuth].[dbo].[billingLog] ([type],[companyID],[sum])VALUES('2','{$val['id']}','{$val['tariff']}');";
                            }
                            if ($val['lock'] == '1') {
                                $sql .= "UPDATE [globalAuth].[dbo].[users] SET [hash]=NULL WHERE [companyID]='{$val['id']}';";
                            }
                        } elseif ($val['lock'] == '0') { //Блокировка из-за недостатка средств
                            $sql .= "
                        UPDATE [globalAuth].[dbo].[company] SET [lock]=1 WHERE [id]='{$val['id']}';
                        UPDATE [globalAuth].[dbo].[users] SET [hash]=NULL WHERE [companyID]='{$val['id']}';
                        ";
                        }
                    } else {//скрипт списания не работал несколько дней
                        $sumDebit = $val['tariff'] * $sumDay;
                        if ($val['balance'] < $sumDebit) { //Списание до вчерашнего дня и блокировка из-за недостатка средств
                            $sumUseDay = $sumDebit - $val['tariff'];
                            $yesterday = date("Y-m-d", strtotime($dateNow) - 86400);
                            $sql .= "
                        UPDATE [globalAuth].[dbo].[company] SET [balance]=[balance]-{$sumUseDay},[dateDebit]='$yesterday',[lock]=1 WHERE [id]='{$val['id']}';
                        UPDATE [globalAuth].[dbo].[users] SET [hash]=NULL WHERE [companyID]='{$val['id']}';
                        ";
                            if ($sumUseDay > 0) {//логирую
                                $sql .= "INSERT INTO [globalAuth].[dbo].[billingLog] ([type],[companyID],[sum])VALUES('2','{$val['id']}','$sumUseDay');";
                            }
                        } else {//списание за несколько дней
                            $sql .= "UPDATE [globalAuth].[dbo].[company] SET [balance]=[balance]-{$sumDebit},[dateDebit]='$dateNow' WHERE [id]='{$val['id']}';";
                            if ($sumDebit > 0) {//логирую
                                $sql .= "INSERT INTO [globalAuth].[dbo].[billingLog] ([type],[companyID],[sum])VALUES('2','{$val['id']}','$sumDebit');";
                            }
                        }
                    }
                }
            }
            $res = qry::queryExec($sql);

            flock($f, LOCK_UN);
            return 'success';
        }else{
            return 'lock';
        }

    }

    //заглушка для ривг функции отправки фото
    public function stubPhoto(){
        return '784523784523';
    }

    //Изменение временных пользователей
    public function deleteGuest(){
        $date = time();
        $sql = "
        DELETE [globalAuth].[dbo].[userObjects] WHERE [timeDelete] IS NOT NULL AND [timeDelete]<=$date;
        UPDATE [globalAuth].[dbo].[users] SET [access]=0,[timeDelete]=NULL,[hash]=NULL WHERE [timeDelete] IS NOT NULL AND [timeDelete]<=$date;
        ";
        return array('set'=>qry::queryExec($sql));
    }

    //Удаление не подтверждённых пользователей на inMarket
    public function deleteNoConfirmUsers(){
        $date = date("Y-m-d H:i:s", strtotime("-1 day"));
        $sql = "DELETE [globalAuth].[dbo].[users] WHERE [dateTimeS]<'$date' AND [companyID]=104 AND [confirm]=1";
        return array('set'=>qry::queryExec($sql));
    }

    //Curl запрос POST
    public static function fetchPOST($server,$fields){
        $ch = curl_init($server);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields,'','&'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    //регистрация пользователя на inMarket
    public function regUserInMarket(){
        global $pg;
        global $saltPass;
        $data = json_decode($pg['data'], true);
        $file = "./list-device/{$pg['captchaID']}.txt";
        $key = "";
        if(file_exists($file)){
            $key = file_get_contents($file);
            file_put_contents($file, "");
        }
        if($key=="" || $data['captcha']!=$key){
            return array('error' => 'Код с картинки не совпадает!', 'cod'=>1);
        }
        if (qry::queryExist("SELECT login FROM [globalAuth].[dbo].[users] WHERE login='{$data['login']}';")) {
            return array('error' => 'Пользователь с таким логином уже существует!', 'cod'=>2);
        }
        if (qry::queryExist("SELECT email FROM [globalAuth].[dbo].[users] WHERE email='{$data['email']}';")) {
            return array('error' => 'Пользователь с таким email уже существует!', 'cod'=>3);
        }
        $data['access'] = '1.00';
        $data['companyID'] = '104';
        $data['confirm'] = '1';
        $data['hash'] = md5($saltPass.$data['login'].$saltPass);
        $fields = [
            'login',
            'password',
            'surName',
            'firstName',
            'lastName',
            'email',
            'phone',
            'access',
            'companyID',
            'confirm',
            'hash'
        ];
        $key = "";
        $values = "";
        foreach ($fields as $field) {
            if ($key) $key .= ",";
            $key .= "[$field]";
            if ($values) $values .= ",";
            $values .= $data[$field] == "" ? "NULL" : "'{$data[$field]}'";
        }
        $sql = "INSERT INTO [globalAuth].[dbo].[users]($key)VALUES($values);";

        $link = "http://194.87.99.148:8080/?rout=auth&func=confirmUser&siteID={$pg['siteID']}&key={$data['hash']}";
        $to = $data['email'];
        $body = "
        <p>Для того чтобы завершить регистрацию перейдите по ссылке:</p>
        <a href=\"{$link}\">$link</a>
        ";
        $topic = 'Подтверждение регистрации в inMarket';
        $send = $this->sendMailSMTP($to,$body,$topic);

//        $send = $this->fetchPOST("http://45.132.18.181:8080", [
//            "json" => json_encode([
//                'send'=>$to,
//                'body'=>$body,
//                'topic'=>$topic,
//            ],JSON_UNESCAPED_UNICODE)
//        ]);
//        $send = json_decode($send,true)['status'];

        return array( 'set' => $send=='ok' && qry::queryExec($sql) , 'smtp'=>$send);
    }

    //подтверждение регистрации на inMarket
    public function confirmUser(){
        global $pg;
        $sql = "UPDATE [globalAuth].[dbo].[users] SET [confirm]=0 WHERE [hash]='{$pg['key']}'";
        $res = qry::queryExec($sql,1,false,true)>0 ? 'success' : 'error';
        header("Location: http://devmobile.stak63.com:8083/inMarket/?siteID={$pg['siteID']}#/registration/$res");
    }

    //отправка почты через SMTP
    public function sendMailSMTP($to,$body,$topic){
        require_once './PHPMailer/src/Exception.php';
        require_once './PHPMailer/src/PHPMailer.php';
        require_once './PHPMailer/src/SMTP.php';

        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';

// Настройки SMTP
        $mail->isSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPDebug = 0;

        $mail->Host = 'ssl://smtp.mail.ru';
        $mail->Port = 465;
        $mail->Username = 'robot1@indrive-tech.com';
        $mail->Password = '1jvywVvdZbjAnit0edJ2';

// От кого
        $mail->setFrom('robot1@indrive-tech.com', 'inMarket');

// Кому
        $mail->addAddress($to);

// Тема письма
        $mail->Subject = $topic;

// Тело письма
        $mail->msgHTML($body);

// Настройки ssl
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        return $mail->send() ? 'ok' : $mail->ErrorInfo;
    }

    //генерация случайной строки
    function generate_string($input, $strength = 16) {
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
    }

    //очистка устаревшей капчи
    function clear_old_device(){
        $dir = "list-device/";
        $filename = "time-clear-device.txt";
        $lastDate = (file_exists($filename)) ? file_get_contents($filename) : time() - 86500;
        $diff = (time() - $lastDate) / 86400;
        if ($diff > 1) {
            file_put_contents($filename, time());
            if (file_exists($dir)) {
                foreach(scandir($dir) as $value){if($value != '.' && $value != '..'){
                    $diff = (time() - filemtime($dir . $value)) / 60;
                    if ($diff > 10) unlink($dir . $value);
                }}
            }
        }
    }

    //формирование изображения для капчи
    public function getCaptcha(){
        global $pg;
        $dir = "list-device";
        if (!file_exists($dir)) mkdir($dir, null, true);
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz';
        //генерируем ключ сессии если нет
        $response = [];
        $captchaID = $pg["captchaID"];
        if($captchaID=='' || !file_exists("$dir/$captchaID.txt")){
            $captchaID='';
            while ($captchaID=='' || file_exists("$dir/$captchaID.txt")) {
                $captchaID = $this->generate_string($chars, 32);
            }
            $response['captchaID'] = $captchaID;
        }
        //генерируем код капчи
        $key = $this->generate_string($chars, 6);
        //создаём сессию и записываем в неё код капчи
        file_put_contents("$dir/$captchaID.txt", $key);
        //создаём изображение с капчей
        $image = imagecreatefrompng('./captcha/bg.png');
        $size = 36;
        $color = imagecolorallocate($image, 66, 182, 66);
        $font = $_SERVER['DOCUMENT_ROOT'].'/captcha/oswald.ttf';
        $angle = rand(-10, 10);
        $x = 56;
        $y = 64;
        imagefttext($image, $size, $angle, $x, $y, $color, $font, $key);
        ob_start();
        imagepng($image);
        $response['image'] = "data:image/png;base64," . base64_encode(ob_get_clean());
        imagedestroy($image);
        //чистим старые сессии
        $this->clear_old_device();
        return $response;
    }

    //запрос восстановления пароля на inMarket
    public function restPassInMarket(){
        global $pg;
        global $saltPass;
        $data = json_decode($pg['data'], true);
        $file = "./list-device/{$pg['captchaID']}.txt";
        $key = "";
        if(file_exists($file)){
            $key = file_get_contents($file);
            file_put_contents($file, "");
        }
        if($key=="" || $data['captcha']!=$key){
            return array('error' => 'Код с картинки не совпадает!', 'cod'=>1);
        }
        $user = qry::queryGet("SELECT * FROM [globalAuth].[dbo].[users] WHERE email='{$data['email']}' AND [confirm]=0;")[0];
        if (!$user) {
            return array('error' => 'Пользователя с таким email не существует!', 'cod'=>2);
        }
        if ($user['access']==0) {
            return array('error' => 'Доступ закрыт! Обратитесь к администратору', 'cod'=>3);
        }
        $hash = md5($saltPass.$user['login'].$saltPass);
        $sql = "UPDATE [globalAuth].[dbo].[users] SET [hash]='$hash' WHERE id='{$user['id']}';";

        $link = "http://devmobile.stak63.com:8083/inMarket/?siteID={$pg['siteID']}#/password/set?key=$hash";
        $to = $data['email'];
        $body = "
        <p>Для того чтобы завершить восстановление пароля перейдите по ссылке:</p>
        <a href=\"{$link}\">$link</a>
        ";
        $topic = 'Восстановление пароля';
        $send = $this->sendMailSMTP($to,$body,$topic);

//        $send = $this->fetchPOST("http://45.132.18.181:8080", [
//            "json" => json_encode([
//                'send'=>$to,
//                'body'=>$body,
//                'topic'=>$topic,
//            ],JSON_UNESCAPED_UNICODE)
//        ]);
//        $send = json_decode($send,true)['status'];

        return array( 'set' => $send=='ok' && qry::queryExec($sql) , 'smtp'=>$send);
    }

    //запись нового пароля при восстановление на inMarket
    public function setPassInMarket(){
        global $pg;
        if ($pg['key']=='' || !qry::queryExist("SELECT 1 FROM [globalAuth].[dbo].[users] WHERE hash='{$pg['key']}';")) {
            return array('error' => 'Ссылка не актуальна!', 'cod'=>1);
        }
        $sql = "UPDATE [globalAuth].[dbo].[users] SET [password]='{$pg['password']}' WHERE [hash]='{$pg['key']}';";
        return array( 'set' => qry::queryExec($sql,1,false,true)>0 );
    }

    //отчёт мобильных устройств и команда на обновление
    public function appReport(){
        global $pg;
        $data = json_decode($pg['data'],true);
        if(!$data['IMEI'])return ['success'=>false];
        $find = qry::queryGet("SELECT*FROM [globalAuth].[dbo].[phoneList] WHERE [IMEI]='{$data['IMEI']}'")[0];
        $fields = [
            'app',
            'verWeb',
            'userID',
            'online'
        ];
        $data['online'] = time();
        $sql = "";
        if($find){
            $sql = "
            UPDATE [globalAuth].[dbo].[phoneList]
            SET
            [update] = 0
            ";
            foreach ($fields as $field){
                $sql .= ($data[$field]=="") ? ",[$field]=NULL" : ",[$field]='{$data[$field]}'";
            }
            $sql .= " WHERE [IMEI] = '{$data['IMEI']}';";
        }else{
            $keys = "[IMEI]";
            $values = "'{$data['IMEI']}'";
            foreach ($fields as $field){
                $keys .= ",[$field]";
                $values .= ($data[$field]=="") ? ",NULL" : ",'{$data[$field]}'";
            }
            $sql = "INSERT INTO [globalAuth].[dbo].[phoneList] ($keys)VALUES($values);";
        }
        return [
            'success' => qry::queryExec($sql),
            'update' => $find['update']=='1',
        ];
    }

    //получить список людей по объекту
    function getUsersByObject(){
        global $pg;
        $valid = $pg['valid']=='true' ? "AND t2.status = 'success'" : "";
        if($pg['token']=='4d515ca9bf22ba77cdd426ece0bb0ec1'){
            $sql = "
  SELECT
       t1.[id]
      ,t1.[surName]
      ,t1.[firstName]
      ,t1.[lastName]
      ,t1.[companyID]
	  ,t3.[name] as [companyName]
  FROM [globalAuth].[dbo].[users] t1
  LEFT JOIN [globalAuth].[dbo].[company] t3 ON t3.id = t1.[companyID]
  INNER JOIN [globalAuth].[dbo].[userObjects] t2 ON t1.id = t2.userID
  $valid
  AND t2.objectID = {$pg['objectID']}
  WHERE [photo] IS NOT NULL
  AND [confirm] = 0
            ;";
            $array = qry::queryGet($sql);
            return [
                'success' => true,
                'count' => count($array),
                'data' => $array,
            ];
        }else{
            return [
                'success' => false,
                'error' => 'Не верный токен!',
            ];
        }
    }
}