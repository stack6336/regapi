<?php
class db {
/**
 * Подключение к БД. Возвращает PDO объект
 */

    public static function getConnection() { 
        try {
            $db = null;
            $dsn = "sqlsrv:Server={$GLOBALS['PARAM']['dbHostMSSQL']},{$GLOBALS['PARAM']['dbPortMSSQL']};Database={$GLOBALS['PARAM']['dbNameMSSQL']}";
            
            $db  = new PDO($dsn, $GLOBALS['PARAM']['dbUserMSSQL'], $GLOBALS['PARAM']['dbPassMSSQL']);
            // Используем исключения
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
        } catch (PDOException $Exception) {
            //Выводим сообщение об исключении.
             var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }

        return $db;
    }

    public static function getConnection2() {
        try {
            $db = null;
            $dsn = "sqlsrv:Server={$GLOBALS['PARAM']['dbHostMSSQL2']},{$GLOBALS['PARAM']['dbPortMSSQL2']};Database={$GLOBALS['PARAM']['dbNameMSSQL2']}";

            $db  = new PDO($dsn, $GLOBALS['PARAM']['dbUserMSSQL2'], $GLOBALS['PARAM']['dbPassMSSQL2']);
            // Используем исключения
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->setAttribute(PDO::SQLSRV_ATTR_FETCHES_NUMERIC_TYPE, true);

        } catch (PDOException $Exception) {
            //Выводим сообщение об исключении.
            // var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }
        return $db;
    }

    public static function getConnection3() {
        try {
            $db = null;
            $dsn = "sqlsrv:Server={$GLOBALS['PARAM']['dbHostMSSQL3']},{$GLOBALS['PARAM']['dbPortMSSQL3']};Database={$GLOBALS['PARAM']['dbNameMSSQL3']}";

            $db  = new PDO($dsn, $GLOBALS['PARAM']['dbUserMSSQL3'], $GLOBALS['PARAM']['dbPassMSSQL3']);
            // Используем исключения
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $Exception) {
            //Выводим сообщение об исключении.
            // var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }
        return $db;
    }


}
