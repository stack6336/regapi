<?php
class sites {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Получить списки
    public function getList(){
        auth::giveAccess(1.5);
        return [
            'sites' => $this->getSites(),
            'company' => $this->getCompany()
        ];
    }

    //Получить компании
    public function getCompany(){
        auth::giveAccess(1.5);
        $where = "";
        if($_SESSION['auth']['info']['access']==1.5){
            $where = ($_SESSION['auth']['info']['companyHead'] == "1")
                ? "WHERE t1.INN='{$_SESSION['auth']['info']['INN']}'"
                : "WHERE t1.id='{$_SESSION['auth']['info']['companyID']}'";
        }
        $sql = "
                SELECT
                t1.[id]
                ,t1.[name]
                ,t1.[balance]
                ,t1.[tariff] as tariffCompany
                ,t2.tariff as tariffProject
                ,JSON_VALUE(t2.[settings], '$.tariff') as tariffSite
                FROM [globalAuth].[dbo].[company] t1
                INNER JOIN [globalAuth].[dbo].[access] t2 ON t2.companyID = t1.id
                AND idProject = (
					SELECT id FROM [globalAuth].[dbo].[project]
					WHERE [name] = 'inMarket'
				)
				$where
                ORDER BY id DESC
        ;";
        return qry::queryGet($sql);
    }

    //Получить сайты
    public function getSites(){
        auth::giveAccess(1.5);
        $where = "";
        if($_SESSION['auth']['info']['access']==1.5){
            $where = ($_SESSION['auth']['info']['companyHead'] == "1")
                ? "WHERE companyID IN (SELECT id FROM [globalAuth].[dbo].[company] WHERE INN = '{$_SESSION['auth']['info']['INN']}')"
                : "WHERE companyID='{$_SESSION['auth']['info']['companyID']}'";
        }
        $sql = "
        SELECT*FROM [globalAuth].[dbo].[sites]
        $where
        ORDER BY id DESC
        ;";
        return qry::queryGet($sql);
    }

    //Удалить сайт
    public function deleteSite(){
        auth::giveAccess(1.5);
        $data = json_decode($this->pg['data'],true);
        $sql = "
        DELETE [globalAuth].[dbo].[sites] WHERE id={$data['id']};
        UPDATE [globalAuth].[dbo].[access]
        SET [tariff] = CAST(JSON_VALUE([settings], '$.tariff') as NUMERIC(18,2))*(
             SELECT COUNT(1) FROM [globalAuth].[dbo].[sites]
             WHERE [companyID]='{$data['companyID']}'
        )
        WHERE companyID = '{$data['companyID']}'
        AND idProject = (SELECT id FROM [globalAuth].[dbo].[project] WHERE [name] = 'inMarket');
        UPDATE [globalAuth].[dbo].[company]
        SET [tariff]=(
             SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
             WHERE companyID = '{$data['companyID']}'
        )
        WHERE id='{$data['companyID']}';
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '3',
        '{$data['companyID']}',
        (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$data['companyID']}'),
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
        return array('set'=>qry::queryExec($sql), 'data'=>$this->getList());
    }

    //Добавить/изменить сайт
    public function setSite(){
        auth::giveAccess(1.5);
        $data = json_decode($this->pg['data'],true);
        if($data['id']){
            $sql = "
            UPDATE [globalAuth].[dbo].[sites]
            SET
            [name]='{$data['name']}',
            [description]='{$data['description']}'
            WHERE id={$data['id']};
            ";
        }else {
            $project = qry::queryGet("
                  SELECT
                  id
                  ,JSON_VALUE([settings], '$.tariff') as tariff
                  FROM [globalAuth].[dbo].[access]
                  WHERE companyID = '{$data['companyID']}'
                  AND idProject = (SELECT id FROM [globalAuth].[dbo].[project] WHERE [name] = 'inMarket'); 
            ")[0];
            if($project['tariff']>0){
                $balance = qry::queryGet("SELECT balance FROM [globalAuth].[dbo].[company] WHERE id='{$data['companyID']}'")[0]['balance'];
                if($balance < $project['tariff'])return ['error'=>'Не достаточно средств!'];
            }
            $sql = "
        INSERT INTO [globalAuth].[dbo].[sites](
         [companyID]
        ,[name]
        ,[description]
        )VALUES(
        '{$data['companyID']}',
        '{$data['name']}',
        '{$data['description']}'
        );
            ";
            if ($project['tariff'] > 0) {
                $sql .= "
        UPDATE [globalAuth].[dbo].[access]
        SET [tariff] = CAST(JSON_VALUE([settings], '$.tariff') as NUMERIC(18,2))*(
             SELECT COUNT(1) FROM [globalAuth].[dbo].[sites]
             WHERE [companyID]='{$data['companyID']}'
        )
        WHERE id = '{$project['id']}';
        UPDATE [globalAuth].[dbo].[company]
        SET [balance]=[balance]-{$project['tariff']},
        [tariff]=(
             SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
             WHERE companyID = '{$data['companyID']}'
        )
        WHERE id='{$data['companyID']}';
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '4',
        '{$data['companyID']}',
        '{$project['tariff']}',
        '{$_SESSION['auth']['info']['id']}'
        );
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '3',
        '{$data['companyID']}',
        (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$data['companyID']}'),
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
            }
        }
        return array('set'=>qry::queryExec($sql), 'data'=>$this->getList());
    }
}