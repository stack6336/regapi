<?php
class company {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //получение компаний
    static function getCompany(){
        $info = "";
        if($_SESSION['auth']['info']['access']>=2){
            $info = ",[balance],[tariff],[lock],[services]";
        }elseif($_SESSION['auth']['info']['access']==1.5){
            $when = ($_SESSION['auth']['info']['companyHead']=="1")
                ? "[INN]='{$_SESSION['auth']['info']['INN']}'"
                : "[id]='{$_SESSION['auth']['info']['companyID']}'";
            $info = "
            ,CASE WHEN ($when) THEN [balance] ELSE NULL END as [balance]
            ,CASE WHEN ($when) THEN [tariff] ELSE NULL END as [tariff]
            ,CASE WHEN ($when) THEN [lock] ELSE NULL END as [lock]
            ,CASE WHEN ($when) THEN [services] ELSE NULL END as [services]
            ";
        }
        $where = "";
        if($_SESSION['auth']['info']['access']==1.5){
            $where = ($_SESSION['auth']['info']['companyHead']=="1")
                ? "WHERE [INN]='{$_SESSION['auth']['info']['INN']}'"
                : "WHERE [id]='{$_SESSION['auth']['info']['companyID']}'";
            $where .= " OR [id] IN (
              SELECT [companyID] FROM [globalAuth].[dbo].[companyObjects]
              WHERE [objectID] IN (
                   SELECT [objectID] FROM [globalAuth].[dbo].[userObjects]
                   WHERE [userID]='{$_SESSION['auth']['info']['id']}'
              )
            )";
        }
        $sql = "
        SELECT
        [id]
      ,[name]
      ,[INN]
      ,[KPP]
      ,[head]
      $info
       FROM [globalAuth].[dbo].[company]
       $where
       ORDER BY id DESC
        ";
        return qry::queryGet($sql);
    }

    //Настройка сервисов для компании
    public function setServices(){
        auth::giveAccess(1.5);
        $sql = "
        UPDATE [globalAuth].[dbo].[company]
        SET [services] = '{$this->pg['data']}'
        WHERE [id] = '{$this->pg['id']}'
        ;";
        $array = ['set'=>qry::queryExec($sql)];
        if($array['set'])$array['data']=$this->getCompany();
        return $array;
    }

    //Получение настроек сервисов
    public function getServices(){
        auth::giveAccess(3);
        $company = qry::queryGet("SELECT [id],[services] FROM [globalAuth].[dbo].[company] WHERE [services] IS NOT NULL;");
        $arr = [];
        foreach ($company as $k=>$v){
            $json = json_decode($v['services'],true);
            foreach ($json as $key=>$val){
                if($val['set']) {
                    if(!is_array($arr[$key]))$arr[$key] = [];
                    $val['attrs']['cname'] = $v['id'];
                    array_push($arr[$key],$val['attrs']);
                }
            }
        }
        return $arr;
    }

    //Существование пользователей у компании
    public function someUsers(){
        $sql = "SELECT TOP(1) 1 FROM [globalAuth].[dbo].[users] WHERE [companyID]='{$this->pg['companyID']}'";
        return ['exist'=>qry::queryExist($sql)];
    }

    //Удаление компании
    public function deleteCompany(){
        auth::giveAccess(2);
        $this->syncUser($this->pg['id'],$this->pg['idCompanyNew']);
        $sql = "
        DELETE [globalAuth].[dbo].[userObjects]
        WHERE [userID] IN (SELECT [id] FROM [globalAuth].[dbo].[users] WHERE companyID='{$this->pg['id']}')
        AND [objectID] NOT IN (
             SELECT [objectID] FROM [globalAuth].[dbo].[companyObjects]
             WHERE [companyID] = '{$this->pg['idCompanyNew']}'
        );
        DELETE [globalAuth].[dbo].[companyObjects] WHERE [companyID]='{$this->pg['id']}';
        UPDATE [globalAuth].[dbo].[users] SET companyID='{$this->pg['idCompanyNew']}',hash=NULL WHERE companyID='{$this->pg['id']}';
        DELETE [globalAuth].[dbo].[access] WHERE companyID='{$this->pg['id']}';
        DELETE [globalAuth].[dbo].[company] WHERE id='{$this->pg['id']}';
        ";
        if($this->pg['newHead'])$sql .= "UPDATE [globalAuth].[dbo].[company] SET head='1' WHERE id='{$this->pg['newHead']}';";
        return array(
            'set'=>qry::queryExec($sql),
            'company'=>$this->getCompany()
        );
    }

    //Пополнение баланса компании
    public function addBalance(){
        auth::giveAccess(2);
        $sql = "
        UPDATE [globalAuth].[dbo].[company]
        SET [balance]=[balance]+{$this->pg['sum']}
        WHERE id='{$this->pg['companyID']}';
        IF @@ROWCOUNT > 0
        INSERT INTO [globalAuth].[dbo].[billingLog]
        (
       [type]
      ,[companyID]
      ,[sum]
      ,[userID]
        )VALUES(
        '1',
        '{$this->pg['companyID']}',
        '{$this->pg['sum']}',
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
        $array = ['set'=>qry::queryExec($sql)];
        if($array['set']){
            auth::debit();
            $array['company'] = $this->getCompany();
        }
        return $array;
    }

    //Получение баланса компании
    static function getBalance(){
        auth::giveAccess(1.5);
        return ['balance'=>qry::queryGet("SELECT [balance] FROM [globalAuth].[dbo].[company] WHERE id='{$_SESSION['auth']['info']['companyID']}';")[0]['balance']];
    }

    //Добавить/изменить компанию
    public function setCompany(){
        auth::giveAccess(2);
        $data = json_decode($this->pg['data']);
        if(qry::queryExist("SELECT * FROM [globalAuth].[dbo].[company] WHERE id!='{$data->id}' AND ([name]='{$data->name}' OR ([INN]='{$data->INN}' AND [KPP]='{$data->KPP}'));"))return array('set'=>2);
        $sql = "";
        if($data->head=='1')$sql .= "UPDATE [globalAuth].[dbo].[company] SET head='0' WHERE INN='{$data->INN}' AND head='1';";
        if($data->id){
            if($this->pg['newHead'])$sql .= "UPDATE [globalAuth].[dbo].[company] SET head='1' WHERE id='{$this->pg['newHead']}';";
            $sql .= "
            UPDATE [globalAuth].[dbo].[company]
            SET
       [name]='{$data->name}'
      ,[INN]='{$data->INN}'
      ,[KPP]='{$data->KPP}'
      ,[head]='{$data->head}'
            WHERE id={$data->id}
            ";
        }else {
            $sql .= "
        INSERT INTO [globalAuth].[dbo].[company](
        [name]
       ,[INN]
       ,[KPP]
       ,[head]
        )VALUES(
        '{$data->name}',
        '{$data->INN}',
        '{$data->KPP}',
        '{$data->head}'
        );
        ";
        }
        $res = qry::queryExec($sql) ? 1 : 0;
        return array('set'=>$res, 'data'=>$this->getCompany());
    }

    //Создание файла синхронизации при смене компании у юзеров
    public function syncUser($idOld,$idNew){//запись файла txt для синхронизации
        $info = qry::queryGet("
        SELECT
       [id] as companyID
      ,[INN]
      ,[KPP]
      ,[name] as company
FROM [globalAuth].[dbo].[company]
WHERE id=$idNew
        ;")[0];
        $info['oldCompanyID'] = $idOld;
        $info['active'] = 3;
        $json = json_encode($info,JSON_UNESCAPED_UNICODE);
        $dir = "C:/userEdit/";
        $name = date('dmYHis')."_$idOld.txt";
        if (!file_exists($dir)) {
            mkdir($dir, null, true);
        }
        $fp = fopen($dir.$name, "w");
        fwrite($fp, $json);
        fclose($fp);
    }
}