<?php
class objects {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Получить объекты
    static function getObjects(){
        auth::giveAccess(2);
        $sql = "SELECT*FROM [globalAuth].[dbo].[objects] ORDER BY id DESC";
        return qry::queryGet($sql);
    }

    //Удалить объект
    public function deleteObject(){
        auth::giveAccess(2);
        $sql = "
        DELETE [globalAuth].[dbo].[objects] WHERE id={$this->pg['id']};
        DELETE [globalAuth].[dbo].[companyObjects] WHERE [objectID]={$this->pg['id']};
        DELETE [globalAuth].[dbo].[userObjects] WHERE [objectID]={$this->pg['id']};
        ";
        return array('set'=>qry::queryExec($sql), 'data'=>$this->getObjects());
    }

    //Добавить/изменить объект
    public function setObject(){
        auth::giveAccess(2);
        $data = json_decode($this->pg['data'],true);
        if($data['id']){
            $sql = "
            UPDATE [globalAuth].[dbo].[objects]
            SET
       [title]='{$data['title']}',
       [description]='{$data['description']}'
            WHERE id={$data['id']}
            ";
        }else {
            $sql = "
        INSERT INTO [globalAuth].[dbo].[objects](
        [title]
       ,[description]
        )VALUES(
        '{$data['title']}',
        '{$data['description']}'
        );
        ";
        }
        return array('set'=>qry::queryExec($sql), 'data'=>$this->getObjects());
    }

    //Прикрепить объект в faceID
    public function addObjectFace(){
        auth::giveAccess(3);
        $sql = "
            UPDATE [globalAuth].[dbo].[objects]
            SET [faceKey]='{$this->pg['faceKey']}'
            WHERE id={$this->pg['objectID']}
            AND [faceKey] IS NULL;
            UPDATE [globalAuth].[dbo].[userObjects] SET [status]=NULL WHERE objectID={$this->pg['objectID']};
            ";
        return array(
            'set'=>qry::queryExec($sql,1,false,true)>0,
            'data'=>$this->getObjectsFace()
        );
    }

    //Получить список объектов в faceID
    static function getObjectsFace(){
        auth::giveAccess(3);
        $sql = "SELECT [id],[title] FROM [globalAuth].[dbo].[objects] WHERE [faceKey] IS NULL ORDER BY id DESC";
        return qry::queryGet($sql);
    }

    //Открепить объект в faceID
    public function delObjectFace(){
        auth::giveAccess(3);
        $sql = "
            UPDATE [globalAuth].[dbo].[objects]
            SET [faceKey]=NULL
            WHERE id={$this->pg['objectID']}
            AND [faceKey]='{$this->pg['faceKey']}';
            UPDATE [globalAuth].[dbo].[userObjects] SET [status]=NULL WHERE objectID={$this->pg['objectID']};
            ";
        return array(
            'set'=>qry::queryExec($sql),
            'data'=>$this->getObjectsFace()
        );
    }
}