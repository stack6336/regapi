<?php
class telegram{
    private $pg;

    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if (method_exists($this, $method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        } else header("HTTP/1.1 404 Not Found");
    }

    //Запись групп и сообщений Telegram в БД ривгом
    public function setMsgRivg(){
        if ((isset($_FILES)) && (!empty($_FILES['file']['name']))) {
            $data = trim(file_get_contents($_FILES['file']['tmp_name']));
            $data = iconv("Windows-1251", "UTF-8", $data);
            $data = json_decode($data, true);

            $sql = "";
            foreach ($data['result'] as $val) {
                if(!empty($val['my_chat_member']) && $val['my_chat_member']['new_chat_member']['status']=='left'){
                    $msg = $val['my_chat_member'];
                    $sql .= "
                    DELETE [globalAuth].[dbo].[telegramGroup] WHERE [groupID] = '{$msg['chat']['id']}' AND [token] = '{$data['token']}';
                    DELETE [globalAuth].[dbo].[telegramMsg] WHERE [groupID] = '{$msg['chat']['id']}' AND [token] = '{$data['token']}';
                    ";
                }elseif (!empty($val['message']) && !array_key_exists('left_chat_member',$val['message'])) {
                    $msg = $val['message'];

                    $sql .= "
            IF NOT EXISTS (SELECT 1 FROM [globalAuth].[dbo].[telegramGroup] WHERE [groupID] = '{$msg['chat']['id']}' AND [token] = '{$data['token']}')
            INSERT INTO [globalAuth].[dbo].[telegramGroup] ([groupID],[token],[title])VALUES('{$msg['chat']['id']}','{$data['token']}','{$msg['chat']['title']}');
            ";

                    if (array_key_exists('new_chat_title', $msg)) $sql .= "
            UPDATE [globalAuth].[dbo].[telegramGroup] SET [title]='{$msg['new_chat_title']}' WHERE [groupID] = '{$msg['chat']['id']}';
            ";

                    elseif (array_key_exists('text', $msg)) $sql .= "
            INSERT INTO [globalAuth].[dbo].[telegramMsg]
            (
             [groupID]
            ,[token]
            ,[typeMSG]
            ,[text]
            ,[dateTime]
            ,[firstName]
            ,[lastName]
            )VALUES(
            '{$msg['chat']['id']}',
            '{$data['token']}',
            'text',
            '" . str_replace("'", "''", $msg['text']) . "',
            '{$msg['date']}',
            '{$msg['from']['first_name']}',
            '{$msg['from']['last_name']}'
            );
            ";

                    elseif (array_key_exists('photo', $msg)) {
                        $caption = ($msg['caption']) ? "'" . str_replace("'", "''", $msg['caption']) . "'" : "NULL";
                        $date = explode(" ", explode("_", $val['fileNameRivg'])[1])[0];
                        $sql .= "
            INSERT INTO [globalAuth].[dbo].[telegramMsg]
            (
             [groupID]
            ,[token]
            ,[typeMSG]
            ,[text]
            ,[dateTime]
            ,[firstName]
            ,[lastName]
            ,[pathFile]
            )VALUES(
            '{$msg['chat']['id']}',
            '{$data['token']}',
            'photo',
             $caption,
            '{$msg['date']}',
            '{$msg['from']['first_name']}',
            '{$msg['from']['last_name']}',
            '/$date/{$val['fileNameRivg']}'
            );
            ";
                    } elseif (array_key_exists('document', $msg)) {
                        $caption = ($msg['caption']) ? "'" . str_replace("'", "''", $msg['caption']) . "'" : "NULL";
                        $date = explode(" ", explode("_", $val['fileNameRivg'])[1])[0];
                        $sql .= "
            INSERT INTO [globalAuth].[dbo].[telegramMsg]
            (
             [groupID]
            ,[token]
            ,[typeMSG]
            ,[text]
            ,[dateTime]
            ,[firstName]
            ,[lastName]
            ,[pathFile]
            ,[fileName]
            )VALUES(
            '{$msg['chat']['id']}',
            '{$data['token']}',
            'document',
             $caption,
            '{$msg['date']}',
            '{$msg['from']['first_name']}',
            '{$msg['from']['last_name']}',
            '/$date/{$val['fileNameRivg']}',
            '{$msg['document']['file_name']}'
            );
            ";
                    }

                }
            }

            return (qry::queryExec($sql)) ? '784523784523' : 'error';
        }
    }

    //Загрузка файла Ривгом
    public function setFileRivg(){
        if ((isset($_FILES)) && (!empty($_FILES['file']['name']))) {
            $date = explode(" ",explode("_",$_FILES['file']['name'])[1])[0];
            $fn = explode(".",$_FILES['file']['name']);
            $dir = "C:/services/telegram/upload/$date";
            if (!file_exists($dir))mkdir($dir, null, true);
            $file = "$dir/{$_FILES['file']['name']}";
            $res = move_uploaded_file($_FILES['file']['tmp_name'], $file);
            if($res && $fn[1]==='jpg'){
                $image = imagecreatefromjpeg($file);
                $width = imagesx($image);
                $height = imagesy($image);
                $new_width = '330';
                $new_height = $new_width/($width/$height);
                $new_image = imagecreatetruecolor($new_width, $new_height);
                imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                imagejpeg($new_image, "$dir/{$fn[0]}_thumb.jpg");
            }
            return $res ? '784523784523' : 'error';
        }
    }

    //Получение списка групп
    public function getGroup(){
        return qry::queryGet("SELECT * FROM [globalAuth].[dbo].[telegramGroup] WHERE [token]='{$this->pg['token']}';");
    }

    //Получение сообщений
    public function getMsg(){
        $where = ($this->pg['lastID']) ? "AND [id]>{$this->pg['lastID']}" : (($this->pg['firstID']) ? "AND [id]<{$this->pg['firstID']}" : "");
        $order = ($this->pg['lastID']) ? "ASC" : "DESC";
        $top = ($this->pg['lastID']) ? "" : "TOP (20)";
        $sql = "
SELECT
$top 
       [id]
      ,[typeMSG]
      ,[text]
      ,[dateTime]
      ,[firstName]
      ,[lastName]
      ,[userID]
      ,[pathFile]
      ,[fileName]
FROM [globalAuth].[dbo].[telegramMsg]
WHERE [groupID]='{$this->pg['groupID']}' AND [token]='{$this->pg['token']}'
$where 
ORDER BY [id] $order;
        ";
        return $this->addSizeImg(qry::queryGet($sql));
    }

    //Вычеслить соотношение сторон фото
    public function addSizeImg($array){
        $dir = "C:/services/telegram/upload";
        foreach ($array as $key=>$value){
            if($value['typeMSG']=='photo' and file_exists($dir.$value['pathFile'])){
                $size = getimagesize($dir.$value['pathFile']);
                $array[$key]['aspect'] = $size[0]/$size[1];
            }
        }
        return $array;
    }

    //Получение токена
    public function getToken(){
        $json = json_decode(qry::queryGet("SELECT [services] FROM [globalAuth].[dbo].[company] WHERE [id]='{$_SESSION['auth']['info']['companyID']}';")[0]['services'],true);
        return ['token'=> ($json['Telegram']['set']) ? $json['Telegram']['attrs']['token'] : null];
    }

    //Отправить сообщение с сайта
    public function setMsg(){
        $result = json_decode($this->pg['result'],true);
        $bots = qry::queryGet("SELECT [token] FROM [globalAuth].[dbo].[telegramGroup] WHERE [groupID]='{$this->pg['groupID']}';");
        $sql = "";
        foreach ($result as $key=>$value) {
            foreach ($bots as $v) {
                $sql .= "
        INSERT INTO [globalAuth].[dbo].[telegramMsg]
        (
       [groupID]
      ,[token]
      ,[userID]
      ,[dateTime]
      ,[text]
      ,[firstName]
      ,[lastName]
      ,[typeMSG]
        ) VALUES (
        '{$this->pg['groupID']}',
        '{$v['token']}',
        '{$_SESSION['auth']['info']['id']}',
        '" . time() . "',
        '" . str_replace("'", "''", $value['text']) . "',
        '{$value['firstName']}',
        '{$value['lastName']}',
        'text'
        );";
            }

            $json = [
                "GroupId" => $this->pg['groupID'],
                "msg" => urlencode($value['text']),
                "token" => $this->pg['token'],
            ];
            $dir = 'C:/services/telegram/s/';
            if (!file_exists($dir))mkdir($dir, null, true);

            $token = str_replace(":","_",$this->pg['token']);

            file_put_contents($dir . time() ."&". rand() . "&" . $token .'.json', json_encode($json, JSON_UNESCAPED_UNICODE));
        }
        $set = qry::queryExec($sql);
        return array(
            'set'=>$set,
            'data'=>($set) ? $this->getMsg() : []
        );
    }

    //Получить фото
    public function getPhoto(){
        $dir = "C:/services/telegram/upload";
        $file = $dir.$this->pg['pathFile'];
        header("Content-type: image/jpeg");
        if (!empty($this->pg['pathFile']) && file_exists($file)){
            if($this->pg['thumb']=='true'){
                $fn = explode(".",$this->pg['pathFile']);
                $path = "{$dir}.{$fn[0]}_thumb.jpg";
                if(file_exists($path))$file=$path;
            }
            if(ob_get_level())ob_end_clean();
            readfile($file);
        }else{
            header("HTTP/1.1 404 Not Found");
        }
    }

    //Загрузить файл
    public function loadFile(){
        $file = 'C:/services/telegram/upload'.$this->pg['pathFile'];
        if (!empty($this->pg['pathFile']) && file_exists($file)){
            header("Content-Disposition:attachment;filename={$this->pg['filename']}");
            if(ob_get_level())ob_end_clean();
            readfile($file);
        }else header("HTTP/1.1 404 Not Found");
    }
}