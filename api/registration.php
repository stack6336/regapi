<?php

class registration
{
    private $pg;
    private $exps = ['jpg', 'jpeg', 'png'];
    private $thumbs = ['50', '200', '500', '1000'];
    private $dir = 'user-photo';

    function __construct($inside = false)
    {
        if ($inside) return false;
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if (method_exists($this, $method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        } else header("HTTP/1.1 404 Not Found");
    }

    //Получить вспомогательные списки СУД
    public function getList()
    {
        return array(
            'project' => ($_SESSION['auth']['info']['access'] >= 2) ? project::getProject() : [],
            'company' => ($_SESSION['auth']['info']['access'] >= 1.5) ? company::getCompany() : [],
            'objects' => ($_SESSION['auth']['info']['access'] >= 2) ? objects::getObjects() : [],
        );
    }

    //Получить список мобильных приложений доступных пользователю
    public function getAppList(){
        auth::giveAccess(1.2);
        return qry::queryGet("
        SELECT
       [title]
      ,[name]
        FROM [globalAuth].[dbo].[project]
        WHERE [id] IN (
             SELECT idProject FROM [globalAuth].[dbo].[access]
             WHERE companyID='{$this->pg['companyID']}'
        )
        AND [mobile] = 1
        ");
    }

    //Получить 10 пользователей
    static function getUsers(){
        auth::giveAccess(1.2);
        global $pg;


        $filters = json_decode($pg['filters'],true);
        $filter = "";

        if($filters['company'])$filter .= "AND users.[companyID]='{$filters['company']}'";
        if($filters['access'])$filter .= "AND users.[access]='{$filters['access']}'";
        $filters['search']=trim($filters['search']);
        if($filters['search']) {
            $filters['search']=preg_replace("/\s+/", " ", $filters['search']);
            $filters['search'] = explode(' ', $filters['search']);
            foreach ($filters['search'] as $search) {
                $filter .= "
        AND (
             users.surName LIKE '%$search%'
             OR users.firstName LIKE '%$search%'
             OR users.lastName LIKE '%$search%'
             OR users.id LIKE '%$search%'
        )
            ";
            }
        }

        $where = "";
        $where2 = "";
        if ($_SESSION['auth']['info']['access'] < 1.5) {
            $where = "AND [companyID]='{$_SESSION['auth']['info']['companyID']}' AND [confirm]=0";
        } elseif ($_SESSION['auth']['info']['access'] == 1.5) {
            $where = ($_SESSION['auth']['info']['companyHead'] == "1")
                ? "AND ( [INN]='{$_SESSION['auth']['info']['INN']}'"
                : "AND ( [companyID]='{$_SESSION['auth']['info']['companyID']}'";
            $where .= " OR [companyID] IN (
              SELECT [companyID] FROM [globalAuth].[dbo].[companyObjects]
              WHERE [objectID] IN (
                   SELECT [objectID] FROM [globalAuth].[dbo].[userObjects]
                   WHERE [userID]='{$_SESSION['auth']['info']['id']}'
              )
            )
            )";
            $where2 = "AND [objectID] IN (SELECT [objectID] FROM [globalAuth].[dbo].[userObjects] WHERE [userID]='{$_SESSION['auth']['info']['id']}')";
        }
        $body = "
        FROM [globalAuth].[dbo].[users]
        LEFT JOIN [globalAuth].[dbo].[company] ON company.id=users.companyID
        WHERE users.id IS NOT NULL
        $where
        ";

        $sql = "SELECT TOP(1) 1 $body AND [confirm]=1";
        $someConfirm = $_SESSION['auth']['info']['access']>=1.5 && qry::queryExist($sql);

        if($filters['confirm'] && $someConfirm){
            $filter .= "AND [confirm]=1";
        }

        $sql = "SELECT COUNT(*) as [count] $body $filter";
        $count = qry::queryGet($sql)[0]['count'];
        $pageCount = ceil($count/10);

        $offset = (($pg['page'] - 1) * 10);
        $login = ($_SESSION['auth']['info']['access'] < 1.5) ? ",CASE WHEN users.id = {$_SESSION['auth']['info']['id']} THEN users.[login] ELSE NULL END as login" : ",users.[login]";
        $timeDelete = ($_SESSION['auth']['info']['access'] >= 1.5) ? ",users.[timeDelete]" : "";
        $sql = "
      SELECT
      users.[id]
      $login
      ,users.[surName]
      ,users.[firstName]
      ,users.[lastName]
      ,users.[email]
      ,users.[phone]
      ,users.[access]
      ,users.[appDefault]
      ,company.[INN]
      ,company.[KPP]
      ,company.[name] as company
      ,users.[companyID]
      ,users.[confirm]
      ,users.[photo]
      ,CASE
      WHEN users.photo IS NULL OR (SELECT COUNT(*) FROM [globalAuth].[dbo].[userObjects] WHERE [userID]=users.id $where2)=0
      THEN NULL
      WHEN EXISTS (SELECT 1 FROM [globalAuth].[dbo].[userObjects] WHERE [userID]=users.id AND [status] IS NOT NULL AND [status]!='success' $where2)
      THEN (SELECT TOP(1) [status] FROM [globalAuth].[dbo].[userObjects] WHERE [userID]=users.id AND [status] IS NOT NULL AND [status]!='success' $where2)
      WHEN EXISTS (SELECT 1 FROM [globalAuth].[dbo].[userObjects] WHERE [userID]=users.id AND [status] IS NULL $where2)
      THEN 'waiting'
      ELSE 'success'
      END as statusFace
      $timeDelete
      $body
      $filter
      ORDER BY users.id DESC
      OFFSET {$offset} ROWS
      FETCH NEXT 10 ROWS ONLY
      ;";
        $users = qry::queryGet($sql);
        return [
            'data' => $users,
            'pageCount' => $pageCount,
            'count' => $count,
            'someConfirm' => $someConfirm,
        ];

    }

    //получить всех пользователей для ГНКТ
    static function getUsersAll(){
        auth::giveAccess(1.2);
        $where = "";
        $where2 = "";
        if($_SESSION['auth']['info']['access'] < 1.5){
            $where = "WHERE [companyID]='{$_SESSION['auth']['info']['companyID']}' AND [confirm]=0";
        }elseif($_SESSION['auth']['info']['access'] == 1.5){
            $where = ($_SESSION['auth']['info']['companyHead']=="1")
                ? "WHERE [INN]='{$_SESSION['auth']['info']['INN']}'"
                : "WHERE [companyID]='{$_SESSION['auth']['info']['companyID']}'";
            $where .= " OR [companyID] IN (
              SELECT [companyID] FROM [globalAuth].[dbo].[companyObjects]
              WHERE [objectID] IN (
                   SELECT [objectID] FROM [globalAuth].[dbo].[userObjects]
                   WHERE [userID]='{$_SESSION['auth']['info']['id']}'
              )
            )";
            $where2 = "AND [objectID] IN (SELECT [objectID] FROM [globalAuth].[dbo].[userObjects] WHERE [userID]='{$_SESSION['auth']['info']['id']}')";
        }
        $login = ($_SESSION['auth']['info']['access'] < 1.5) ? ",CASE WHEN users.id = {$_SESSION['auth']['info']['id']} THEN users.[login] ELSE NULL END as login" : ",users.[login]";
        $timeDelete = ($_SESSION['auth']['info']['access'] >= 1.5) ? ",users.[timeDelete]" : "";
        $sql = "
        SELECT
       users.[id]
      $login
      ,users.[surName]
      ,users.[firstName]
      ,users.[lastName]
      ,users.[email]
      ,users.[phone]
      ,users.[access]
      ,users.[appDefault]
      ,company.[INN]
      ,company.[KPP]
      ,company.[name] as company
      ,users.[companyID]
      ,users.[confirm]
      ,CASE
      WHEN users.photo IS NULL OR (SELECT COUNT(*) FROM [globalAuth].[dbo].[userObjects] WHERE [userID]=users.id $where2)=0
      THEN NULL
      WHEN EXISTS (SELECT 1 FROM [globalAuth].[dbo].[userObjects] WHERE [userID]=users.id AND [status] IS NOT NULL AND [status]!='success' $where2)
      THEN (SELECT TOP(1) [status] FROM [globalAuth].[dbo].[userObjects] WHERE [userID]=users.id AND [status] IS NOT NULL AND [status]!='success' $where2)
      WHEN EXISTS (SELECT 1 FROM [globalAuth].[dbo].[userObjects] WHERE [userID]=users.id AND [status] IS NULL $where2)
      THEN 'waiting'
      ELSE 'success'
      END as statusFace
      $timeDelete
        FROM [globalAuth].[dbo].[users]
        LEFT JOIN [globalAuth].[dbo].[company] ON company.id=users.companyID
        $where
        ORDER BY id DESC
        ;";
        return qry::queryGet($sql);
    }

    //получить список суперадминов и разработчиков
    public function getDevelopers(){
        auth::giveAccess(2);
        return qry::queryGet("
        SELECT
       [id]
      ,[surName]
      ,[firstName]
      ,[lastName]
        FROM
        [globalAuth].[dbo].[users]
        WHERE [access]>=2
        ;");
    }

    //Удаление пользователя из БД
    public function deleteUser()
    {
        auth::giveAccess(3);
        $this->syncUser($this->pg['ID'], 2);
        $this->setLog('DELETE', $this->pg['ID']);
        $sql = "
        DELETE [globalAuth].[dbo].[userObjects] WHERE [userID]={$this->pg['ID']};
        DELETE [globalAuth].[dbo].[users] WHERE id={$this->pg['ID']};
        ";
        $this->deletePhotoUser($this->pg['ID']);
        return array('set' => qry::queryExec($sql), 'users' => $this->getUsers());
    }

    //Проверка на существование пользователя с такими же ФИО
    public function existDouble()
    {
        auth::giveAccess(1.2);
        $data = json_decode($this->pg['data'], true);
        $where = "";
        if ($_SESSION['auth']['info']['access'] < 1.5) {
            $where = "AND [companyID]='{$_SESSION['auth']['info']['companyID']}'";
        } elseif ($_SESSION['auth']['info']['access'] == 1.5) {
            $or = "OR [companyID] IN (
              SELECT [companyID] FROM [globalAuth].[dbo].[companyObjects]
              WHERE [objectID] IN (
                   SELECT [objectID] FROM [globalAuth].[dbo].[userObjects]
                   WHERE [userID]='{$_SESSION['auth']['info']['id']}'
              )
            )";
            $where = ($_SESSION['auth']['info']['companyHead'] == "1")
                ? "AND ([INN]='{$_SESSION['auth']['info']['INN']}' $or)"
                : "AND ([companyID]='{$_SESSION['auth']['info']['companyID']}' $or)";
        }
        return ['res' => qry::queryExist("
            SELECT 1 FROM [globalAuth].[dbo].[users]
            LEFT JOIN [globalAuth].[dbo].[company] ON company.id=users.companyID
            WHERE
            REPLACE([surName],'ё','е')=REPLACE('{$data['surName']}','ё','е')
            AND REPLACE([firstName],'ё','е')=REPLACE('{$data['firstName']}','ё','е')
            AND REPLACE([lastName],'ё','е')=REPLACE('{$data['lastName']}','ё','е')
            AND users.[id]!='{$data['id']}'
            $where
        ;")];
    }

    //Добавление/изменение пользователя
    public function setUser(){
        auth::giveAccess(1.2);
        $data = json_decode($this->pg['data'], true);
        if(!$data['companyID'])return ['set'=>false];
        $data['confirm'] = "0";
        if (
            ($_SESSION['auth']['info']['access'] < 1.5 && (!$data['id'] || $data['id'] != $_SESSION['auth']['info']['id']))
            || ($data['id'] && $_SESSION['auth']['info']['access'] < qry::queryGet("SELECT [access] FROM [globalAuth].[dbo].[users] WHERE id={$data['id']}")[0]['access'])
            || $data['access'] > $_SESSION['auth']['info']['access']
        ) {
            header("HTTP/1.1 403 Forbidden");
            exit;
        }
        if ($_SESSION['auth']['info']['access'] < 1.5) {
            $data['companyID'] = $_SESSION['auth']['info']['companyID'];
        }
        if ($data['login'] && qry::queryExist("SELECT login FROM [globalAuth].[dbo].[users] WHERE login='{$data['login']}' AND id!='{$data['id']}';")) return array('set' => 2);
        if ($data['email'] && qry::queryExist("SELECT email FROM [globalAuth].[dbo].[users] WHERE email='{$data['email']}' AND id!='{$data['id']}';")) return array('set' => 3);
        $fields = [
            'login',
            'password',
            'surName',
            'firstName',
            'lastName',
            'email',
            'phone',
            'access',
            'appDefault',
            'companyID',
            'confirm',
        ];
        if ($_SESSION['auth']['info']['access'] >= 1.5) {
            array_push($fields, 'timeDelete');
        }

        if ($data['id']) {
            $set = "";
            foreach ($fields as $field) {
                if ($field != 'password' || $data['password'] != "") {
                    if ($set) $set .= ",";
                    $set .= "[$field]=";
                    $set .= $data[$field] == "" ? "NULL" : "'{$data[$field]}'";
                }
            }
            $sql = "UPDATE [globalAuth].[dbo].[users] SET $set WHERE id={$data['id']};";
        } else {
            $key = "";
            $values = "";
            foreach ($fields as $field) {
                if ($key) $key .= ",";
                $key .= "[$field]";
                if ($values) $values .= ",";
                $values .= $data[$field] == "" ? "NULL" : "'{$data[$field]}'";
            }
            $sql = "INSERT INTO [globalAuth].[dbo].[users]($key)VALUES($values);";
        }
        $idUser = ($data['id']) ? qry::queryExec($sql) : qry::queryExec($sql, 1, true);
        $res = $idUser ? 1 : 0;
        if ($res == 1 && $data['id']) $auth = auth::setAuth($data['id']);

        $ID = $data['id'] ?: $idUser;

        if ( //работа с объектами OLD
            $res == 1
            && !empty($this->pg['userObjects'])
            && (
                $_SESSION['auth']['info']['access'] >= 2
                || ($_SESSION['auth']['info']['access'] == 1.5 && $_SESSION['auth']['info']['id'] != $data['id'])
            )
        ) {

            $where = ($_SESSION['auth']['info']['access'] == 1.5)
                ? "AND [objectID] IN (SELECT [objectID] FROM [globalAuth].[dbo].[userObjects] WHERE [userID]='{$_SESSION['auth']['info']['id']}')"
                : "";
            $oldObj = qry::queryGet("SELECT*FROM [globalAuth].[dbo].[userObjects] WHERE userID='$ID' $where");
            $newObj = json_decode($this->pg['userObjects'], true);
            $sql = "";
            foreach ($oldObj as $obj) {
                $findIndex = array_search($obj['objectID'], $newObj);
                if ($findIndex !== false) {
                    array_splice($newObj, $findIndex, 1);
                } else {
                    $sql .= "DELETE [globalAuth].[dbo].[userObjects] WHERE [id]='{$obj['id']}';";
                }
            }
            if (count($newObj) > 0) {
                $sql .= "INSERT INTO [globalAuth].[dbo].[userObjects] ([userID],[objectID]) VALUES ";
                foreach ($newObj as $k => $val) {
                    if ($k > 0) $sql .= ",";
                    $sql .= "('$ID','$val')";
                }
                $sql .= ";";
            }
            if ($sql) qry::queryExec($sql);
        }

        if ( //работа с объектами
            $res == 1
            && !empty($this->pg['userObjectsNew'])
            && (
                $_SESSION['auth']['info']['access'] >= 2
                || ($_SESSION['auth']['info']['access'] == 1.5 && $_SESSION['auth']['info']['id'] != $data['id'])
            )
        ) {

            $where = ($_SESSION['auth']['info']['access'] == 1.5)
                ? "AND [objectID] IN (SELECT [objectID] FROM [globalAuth].[dbo].[userObjects] WHERE [userID]='{$_SESSION['auth']['info']['id']}')"
                : "";
            $oldObj = qry::queryGet("SELECT*FROM [globalAuth].[dbo].[userObjects] WHERE userID='$ID' $where");
            $newObj = json_decode($this->pg['userObjectsNew'], true);
            $sql = "";
            foreach ($oldObj as $obj) {
                $findIndex = -1;
                foreach ($newObj as $i=>$e){if($e['id']==$obj['objectID']){$findIndex = $i;break;}}
                if ($findIndex !== -1) {
                    $tD = $newObj[$findIndex]['timeDelete'] == "" ? "NULL" : "'{$newObj[$findIndex]['timeDelete']}'";
                    $sql .= "UPDATE [globalAuth].[dbo].[userObjects] SET [timeDelete]=$tD WHERE [id]='{$obj['id']}';";
                    array_splice($newObj, $findIndex, 1);
                } else {
                    $sql .= "DELETE [globalAuth].[dbo].[userObjects] WHERE [id]='{$obj['id']}';";
                }
            }
            if (count($newObj) > 0) {
                $sql .= "INSERT INTO [globalAuth].[dbo].[userObjects] ([userID],[objectID],[timeDelete]) VALUES ";
                foreach ($newObj as $k => $val) {
                    $tD = $val['timeDelete'] == "" ? "NULL" : "'{$val['timeDelete']}'";
                    if ($k > 0) $sql .= ",";
                    $sql .= "('$ID','{$val['id']}',$tD)";
                }
                $sql .= ";";
            }
            if ($sql) qry::queryExec($sql);
        }

        if ($this->pg['photo'] == "clear") {
            $this->deletePhotoUser($ID);
            qry::queryExec("
                UPDATE [globalAuth].[dbo].[users] SET [photo]=NULL WHERE id=$ID;
                UPDATE [globalAuth].[dbo].[userObjects] SET [status]=NULL WHERE userID=$ID;
            ");
        } else if (!empty($_FILES)) {
            $exp = strtolower(end(explode(".", $_FILES['photo']['name'])));
            if (in_array($exp, $this->exps)) {
                $name = "user_$ID.$exp";
                $file = "{$this->dir}/$name";
                if (!file_exists($this->dir)) {
                    mkdir($this->dir, null, true);
                }
                if (file_exists($_FILES['photo']['tmp_name'])) {
                    $this->deletePhotoUser($ID);
                    if (move_uploaded_file($_FILES['photo']['tmp_name'], $file)) $this->setThumb($file);
                    qry::queryExec("
                        UPDATE [globalAuth].[dbo].[users] SET [photo]='" . time() . "' WHERE id=$ID;
                        UPDATE [globalAuth].[dbo].[userObjects] SET [status]=NULL WHERE userID=$ID;
                    ");
                }
            }
        } else if ($data['id'] && $data['photo'] == 'reset') {
            qry::queryExec("
               UPDATE [globalAuth].[dbo].[users] SET [photo]='" . time() . "' WHERE id=$ID AND [photo] IS NOT NULL;
               UPDATE [globalAuth].[dbo].[userObjects] SET [status]=NULL WHERE userID=$ID;
             ");
        }

        if ($res == 1) {
            $this->syncUser($ID, 1);
            $this->setLog($data['id'] ? 'UPDATE' : 'INSERT', $ID, $data['comment']);
        }

        return array('set' => $res, 'idUser' => $ID, 'users' => $this->getUsers());
    }

    //Создание миниатюр фото пользователя
    public function setThumb($file)
    {
        foreach ($this->thumbs as $size) {
            $name = explode(".", $file);
            $path = "{$name[0]}_thumb_$size.{$name[1]}";
            $type = $name[1];
            $image = ($type == 'jpg' || $type == 'jpeg') ? imagecreatefromjpeg($file) : imagecreatefrompng($file);
            $exif = @exif_read_data($file);
            if ($image && $exif && isset($exif['Orientation'])) {
                $ort = $exif['Orientation'];
                if ($ort == 6 || $ort == 5) {
                    $image = imagerotate($image, 270, 0);
                }
                if ($ort == 3 || $ort == 4) {
                    $image = imagerotate($image, 180, 0);
                }
                if ($ort == 8 || $ort == 7) {
                    $image = imagerotate($image, 90, 0);
                }
                if ($ort == 5 || $ort == 4 || $ort == 7) {
                    imageflip($image, IMG_FLIP_HORIZONTAL);
                }
            }
            $width = imagesx($image);
            $height = imagesy($image);
            if (($height >= $width && $width > $size) || ($height < $width && $height > $size)) {
                $new_width = ($height >= $width) ? $size : $size / ($height / $width);
                $new_height = ($height >= $width) ? $size / ($width / $height) : $size;
                $new_image = imagecreatetruecolor($new_width, $new_height);
                if($type == 'png'){
                    imagealphablending($new_image, false);
                    imagesavealpha($new_image, true);
                }
                imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                ($type == 'jpg' || $type == 'jpeg') ? imagejpeg($new_image, $path) : imagepng($new_image, $path);
                imagedestroy($new_image);
            }
        }
    }

    //Удаление фото пользователя
    public function deletePhotoUser($ID)
    {
        foreach ($this->exps as $exp) {
            if (file_exists($path = "{$this->dir}/user_$ID.$exp")) {
                unlink($path);
                foreach ($this->thumbs as $size) {
                    if (file_exists($path = "{$this->dir}/user_{$ID}_thumb_{$size}.$exp")) unlink($path);
                    else break;
                }
                break;
            }
        }
    }

    //Запись логов действий с пользователями
    public function setLog($action, $userID, $comment = '')
    {
        $data = qry::queryGet("SELECT*FROM [globalAuth].[dbo].[users] WHERE id=$userID;")[0];
        $fields = [
            'creatorID' => $_SESSION['auth']['info']['id'],
            'userID' => $userID,
            'data' => json_encode($data, JSON_UNESCAPED_UNICODE),
            'comment' => $comment,
            'action' => $action,
        ];
        $keys = "";
        $values = "";
        foreach ($fields as $key => $value) {
            if ($keys) $keys .= ",";
            $keys .= "[$key]";
            if ($values) $values .= ",";
            $values .= $value == "" ? "NULL" : "'$value'";
        }
        $sql = "INSERT INTO [globalAuth].[dbo].[usersLog]($keys)VALUES($values);";
        return qry::queryExec($sql);
    }

    //Создание файлов для синхронизации действий с пользователями
    public function syncUser($id, $active)
    {//запись файла txt для синхронизации
        $userInfo = qry::queryGet("
        SELECT
       users.[id]
      ,users.[login]
      ,users.[surName]
      ,users.[firstName]
      ,users.[lastName]
      ,users.[email]
      ,users.[companyID]
      ,company.[INN]
      ,company.[KPP]
      ,company.[name] as company
FROM [globalAuth].[dbo].[users]
LEFT JOIN [globalAuth].[dbo].[company] ON company.id=users.companyID
WHERE users.id=$id
        ;")[0];
        $userInfo['active'] = $active;
        $json = json_encode($userInfo, JSON_UNESCAPED_UNICODE);
        $dir = "C:/userEdit/";
        $name = date('dmYHis') . "_$id.txt";
        if (!file_exists($dir)) {
            mkdir($dir, null, true);
        }
        $fp = fopen($dir . $name, "w");
        fwrite($fp, $json);
        fclose($fp);
    }
}