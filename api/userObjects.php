<?php
class userObjects {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Получить список объектов пользователя и объектов компании OLD
    public function getUserObjects(){
        auth::giveAccess(1.5);
        if($_SESSION['auth']['info']['access']>=2) {
            $sql1 = "
              SELECT
              t1.[id],
              t1.[title]
              FROM [globalAuth].[dbo].[objects] t1
              INNER JOIN [globalAuth].[dbo].[companyObjects] t2 ON t2.[objectID] = t1.[id]
              AND t2.[companyID]='{$this->pg['companyID']}'
        ;";
            $sql2 = "
              SELECT
              t1.[objectID] as [id]
              FROM [globalAuth].[dbo].[userObjects] t1
              INNER JOIN [globalAuth].[dbo].[companyObjects] t2 ON t2.[objectID] = t1.[objectID]
              AND t2.[companyID] = '{$this->pg['companyID']}'
              WHERE t1.[userID]='{$this->pg['userID']}'
        ;";
        }else{
            $sql1 = "
              SELECT
              t1.[id],
              t1.[title]
              FROM [globalAuth].[dbo].[objects] t1
              INNER JOIN [globalAuth].[dbo].[companyObjects] t2 ON t2.[objectID] = t1.[id]
              AND t2.[companyID]='{$this->pg['companyID']}'
              INNER JOIN [globalAuth].[dbo].[userObjects] t3 ON t3.[objectID] = t1.[id]
              AND t3.[userID]='{$_SESSION['auth']['info']['id']}'
        ;";
            $sql2 = "
              SELECT
              t1.[objectID] as [id]
              FROM [globalAuth].[dbo].[userObjects] t1
              INNER JOIN [globalAuth].[dbo].[companyObjects] t2 ON t2.[objectID] = t1.[objectID]
              AND t2.[companyID] = '{$this->pg['companyID']}'
              INNER JOIN [globalAuth].[dbo].[userObjects] t3 ON t3.[objectID] = t1.[objectID]
              AND t3.[userID]='{$_SESSION['auth']['info']['id']}'
              WHERE t1.[userID]='{$this->pg['userID']}'
        ;";
        }
        return [
            'companyObjects' => qry::queryGet($sql1),
            'userObjects' => qry::queryGet($sql2),
        ];
    }

    //Получить список объектов пользователя и объектов компании
    public function getUserObjectsNew(){
        auth::giveAccess(1.5);
        $where = $_SESSION['auth']['info']['access']==1.5 ? "
        AND [objectID] IN (
            SELECT [objectID] FROM [globalAuth].[dbo].[userObjects]
            WHERE [userID]='{$_SESSION['auth']['info']['id']}'
        )
        " : "";
        $sql = "
              SELECT
              t1.[id],
              t1.[title],
              CASE WHEN t2.[id] IS NOT NULL THEN 1 ELSE 0 END as [access],
              t2.[timeDelete]
              FROM [globalAuth].[dbo].[objects] t1
              LEFT JOIN [globalAuth].[dbo].[userObjects] t2 ON t2.[objectID] = t1.[id]
              AND [userID] = '{$this->pg['userID']}'
              WHERE t1.[id] IN (
                   SELECT [objectID] FROM [globalAuth].[dbo].[companyObjects]
                   WHERE [companyID]='{$this->pg['companyID']}'
                   $where
              )
        ;";
        return qry::queryGet($sql);
    }

    //Получить список пользователей указанных объектов
    public function getObjectUsers(){
        auth::giveAccess(3);
        $objects = json_decode($this->pg['objects'],true);
        $arr = [];
        foreach ($objects as $objectID) {
            if ($this->pg['faceKey'] && qry::queryExist("SELECT 1 FROM [globalAuth].[dbo].[objects] WHERE [id]=$objectID AND (faceKey IS NULL OR [faceKey]!='{$this->pg['faceKey']}');")) {
                $arr[$objectID] = 'reset';
            } else {
                $sql = "
        SELECT
        [id]
        ,CONCAT(surName,' ',firstName,' ',lastName) as fio
        ,[photo]
        FROM [globalAuth].[dbo].[users]
        WHERE [id] IN (SELECT userID FROM [globalAuth].[dbo].[userObjects] WHERE objectID='$objectID')
        AND [photo] IS NOT NULL
        ";
                $arr[$objectID] = qry::queryGet($sql);
            }
        }
        return $arr;
    }

    //Запись отчёта программы faceID
    public function setReportFace(){
        auth::giveAccess(3);
        $data = json_decode($this->pg['data'],true);
        $ids = "";
        foreach ($data as $objID=>$obj) {
            if($ids)$ids .= ",";
            $ids .= $objID;
        }
        $share = qry::queryGet("
        SELECT
        t1.[id],
        t1.[userID],
        t1.[objectID],
        t1.[status],
        t2.[photo]
        FROM [globalAuth].[dbo].[userObjects] t1
        LEFT JOIN [globalAuth].[dbo].[users] t2 ON t2.id = t1.userID
        WHERE [objectID] IN ($ids);");
        $arr = [];
        foreach ($share as $v){
            if(!$arr[$v['objectID']])$arr[$v['objectID']]=[];
            $arr[$v['objectID']][$v['userID']] = $v;
        }
        $sql = "";
        foreach ($data as $objID=>$obj){
            foreach ($obj['error'] as $val){
                $item = $arr[$objID][$val['id']];
                if($item['photo']==$val['photo'] && $item['status']==null){
                    $sql .= "
                    UPDATE [globalAuth].[dbo].[userObjects]
                    SET [status] = '{$val['error']}'
                    WHERE [id]='{$item['id']}'
                    ;";
                }
            }
            foreach ($obj['success'] as $val){
                $item = $arr[$objID][$val['id']];
                if($item['photo']==$val['photo'] && $item['status']==null){
                    $sql .= "
                    UPDATE [globalAuth].[dbo].[userObjects]
                    SET [status] = 'success'
                    WHERE [id]='{$item['id']}'
                    ;";
                }
            }
        }
        return ($sql) ? qry::queryExec($sql) : true;
    }

    //Сбросить статус объекта
    public function resetObjectStatus(){
        auth::giveAccess(3);
        $sql = "
           IF EXISTS (
               SELECT 1 FROM [globalAuth].[dbo].[objects]
               WHERE [id]='{$this->pg['objectID']}'
               AND [faceKey]='{$this->pg['faceKey']}'
           )
           UPDATE [globalAuth].[dbo].[userObjects]
           SET [status]=NULL
           WHERE [objectID]='{$this->pg['objectID']}'
        ";
        return array('set'=>qry::queryExec($sql));
    }
}