<?php
class companyAuto {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Получить авто привязанные компании
    public function get(){
        auth::giveAccess(1.5);
        $sql = "
        SELECT
       [id]
      ,[carID]
        FROM [globalAuth].[dbo].[companyAuto]
        WHERE [companyID] = '{$this->pg['companyID']}'
        ORDER BY id DESC
        ;";
        return qry::queryGet($sql);
    }

    //Удалить авто компании
    public function delete(){
        auth::giveAccess(1.5);
        $sql = "
        DELETE [globalAuth].[dbo].[companyAuto] WHERE id={$this->pg['id']};
        UPDATE [globalAuth].[dbo].[access]
        SET [tariff] = CAST(JSON_VALUE([settings], '$.tariff') as NUMERIC(18,2))*(
             SELECT COUNT(1) FROM [globalAuth].[dbo].[companyAuto]
             WHERE [companyID]='{$this->pg['companyID']}'
        )
        WHERE companyID = '{$this->pg['companyID']}'
        AND idProject = (SELECT id FROM [globalAuth].[dbo].[project] WHERE [name] = 'wialon');
        UPDATE [globalAuth].[dbo].[company]
        SET [tariff]=(
             SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
             WHERE companyID = '{$this->pg['companyID']}'
        )
        WHERE id='{$this->pg['companyID']}';
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '3',
        '{$this->pg['companyID']}',
        (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'),
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
        return array(
            'set'=>qry::queryExec($sql),
            'data'=>$this->get(),
            'company' => company::getCompany(),
            'access' => access::getAccess(),
        );
    }

    //Добавить авто компании
    public function set(){
        auth::giveAccess(1.5);
            $project = qry::queryGet("
                  SELECT
                  id
                  ,JSON_VALUE([settings], '$.tariff') as tariff
                  FROM [globalAuth].[dbo].[access]
                  WHERE companyID = '{$this->pg['companyID']}'
                  AND idProject = (SELECT id FROM [globalAuth].[dbo].[project] WHERE [name] = 'wialon');
            ")[0];
            if($project['tariff']>0){
                $balance = qry::queryGet("SELECT balance FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'")[0]['balance'];
                if($balance < $project['tariff'])return ['error'=>'Не достаточно средств!'];
            }
            if(qry::queryExist("SELECT 1 FROM [globalAuth].[dbo].[companyAuto] WHERE [carID]='{$this->pg['carID']}' AND [companyID]='{$this->pg['companyID']}'"))return ['error'=>'Такая машина уже привязана!'];
            $sql = "
                 INSERT INTO [globalAuth].[dbo].[companyAuto] (
                 [carID],
                 [companyID],
                 [token]
                 )VALUES(
                 '{$this->pg['carID']}',
                 '{$this->pg['companyID']}',
                 '{$this->pg['token']}'
                 )
            ;";
            if ($project['tariff'] > 0) {
                $sql .= "
        UPDATE [globalAuth].[dbo].[access]
        SET [tariff] = CAST(JSON_VALUE([settings], '$.tariff') as NUMERIC(18,2))*(
             SELECT COUNT(1) FROM [globalAuth].[dbo].[companyAuto]
             WHERE [companyID]='{$this->pg['companyID']}'
        )
        WHERE id = '{$project['id']}';
        UPDATE [globalAuth].[dbo].[company]
        SET [balance]=[balance]-{$project['tariff']},
        [tariff]=(
             SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
             WHERE companyID = '{$this->pg['companyID']}'
        )
        WHERE id='{$this->pg['companyID']}';
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '4',
        '{$this->pg['companyID']}',
        '{$project['tariff']}',
        '{$_SESSION['auth']['info']['id']}'
        );
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '3',
        '{$this->pg['companyID']}',
        (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'),
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
            }
//        }
        return array(
            'set'=>qry::queryExec($sql),
            'data'=>$this->get(),
            'company' => company::getCompany(),
            'access' => access::getAccess(),
        );
    }

    //Проверить существование машин в компании
    public function existAuto(){
        return ['exist'=>qry::queryExist("SELECT 1 FROM [globalAuth].[dbo].[companyAuto] WHERE companyID='{$this->pg['companyID']}' AND token='{$this->pg['token']}'")];
    }

    //Получение машины из виалона проектами
    public function getAuto(){
        $accessAuto = qry::queryGet("SELECT [carID] FROM [globalAuth].[dbo].[companyAuto] WHERE companyID='{$_SESSION['auth']['info']['companyID']}'");
        $carIds = implode(',', array_map(function ($value) {
            return $value['carID'];
        }, $accessAuto));
        return auth::fetchPOST("http://wialonapi.stak63.com", [
            "api" => "Cars",
            "action" => "get",
            "params" => json_encode([
                'carIds' => $carIds
            ]),
            "auth" => json_encode($_SESSION['auth'])
        ]);
    }
}