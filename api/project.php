<?php
class project {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //получить проекты
    static function getProject(){
        auth::giveAccess(2);
            $sql = "
        SELECT
       [id]
      ,[title]
      ,[name]
      ,[icon]
      ,[mobile]
        FROM [globalAuth].[dbo].[project]
        ORDER BY id DESC
        ";
        return qry::queryGet($sql);
    }

    //удалить проект
    public function deleteProject(){
        auth::giveAccess(3);
        if(qry::queryExist("SELECT 1 FROM [globalAuth].[dbo].[access] WHERE idProject={$this->pg['ID']}"))return ['error'=>'Проект привязан к компаниям!'];
        $sql = "
        DELETE [globalAuth].[dbo].[project] WHERE id={$this->pg['ID']};
        ";
        return array('set'=>qry::queryExec($sql), 'project'=>$this->getProject());
    }

    //добавить/изменить проект
    public function setProject(){
        auth::giveAccess(3);
        $data = json_decode($this->pg['data']);
        if(qry::queryExist("SELECT [name] FROM [globalAuth].[dbo].[project] WHERE [name]='{$data->name}' AND id!='{$data->id}';"))return array('set'=>2);
        if($data->id){
            $sql = "
            UPDATE [globalAuth].[dbo].[users] SET appDefault='{$data->name}',hash=NULL WHERE appDefault = (SELECT [name] FROM [globalAuth].[dbo].[project] WHERE id={$data->id});
            UPDATE [globalAuth].[dbo].[project]
            SET
       [title]='{$data->title}'
      ,[name]='{$data->name}'
      ,[icon]='{$data->icon}'
      ,[mobile]='{$data->mobile}'
            WHERE id={$data->id};
            UPDATE [globalAuth].[dbo].[users] SET hash=NULL WHERE companyID IN (SELECT [companyID] FROM [globalAuth].[dbo].[access] WHERE idProject={$data->id});
            ";
        }else {
            $sql = "
        INSERT INTO [globalAuth].[dbo].[project](
        [title]
       ,[name]
       ,[icon]
       ,[mobile]
        )VALUES(
        '{$data->title}',
        '{$data->name}',
        '{$data->icon}',
        '{$data->mobile}'
        );
        ";
        }
        $res = qry::queryExec($sql) ? 1 : 0;
        return array('set'=>$res, 'project'=>$this->getProject());
    }
}