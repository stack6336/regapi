<?php
class access {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Добавить услугу для компании
    public function setAccess(){
        auth::giveAccess(2);
        $data = json_decode($this->pg['data'],true);
        $except = [
            'inMarket'=>'sites',
            'wialon'=>'companyAuto',
            'SKR5.2'=>'companySKR',
            'inMobile'=>'companyApp',
        ];
        if(array_key_exists($data['project'],$except)){
            $count = qry::queryGet("SELECT COUNT(1) as [count] FROM [globalAuth].[dbo].[{$except[$data['project']]}] WHERE [companyID]='{$this->pg['companyID']}'")[0]['count'];
            $data['tariff'] = $data['settings']['tariff']*$count;
        }
        $settings = (!empty($data['settings'])) ? "'".json_encode($data['settings'],JSON_UNESCAPED_UNICODE)."'" : "NULL";
        if($data['id']){
            $sql = "UPDATE [globalAuth].[dbo].[access] SET [tariff] = '{$data['tariff']}', [settings]=$settings WHERE id = '{$data['id']}';";
            if($data['oldTariff']!=$data['tariff']){
                $sql .="
            UPDATE [globalAuth].[dbo].[company]
            SET [tariff]=(
                        SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
                        WHERE companyID = '{$this->pg['companyID']}'
                        )
            WHERE id='{$this->pg['companyID']}';
                INSERT INTO [globalAuth].[dbo].[billingLog]
                ([type],[companyID],[sum],[userID])VALUES(
                '3',
                '{$this->pg['companyID']}',
                (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'),
                '{$_SESSION['auth']['info']['id']}'
                );
                ";
            }
        }else{
            $balance = "";
            if($data['tariff']>0){
                $balance = qry::queryGet("SELECT balance FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'")[0]['balance'];
                if($balance < $data['tariff'])return ['set'=>2]; //Не достаточно средств на балансе компании
            }
                $sql = "
                INSERT INTO [globalAuth].[dbo].[access](
                [companyID]
                ,[idProject]
                ,[tariff]
                ,[settings]
                )VALUES(
                '{$this->pg['companyID']}',
                '{$data['idProject']}',
                '{$data['tariff']}',
                $settings
                );
                UPDATE [globalAuth].[dbo].[users] SET hash=NULL WHERE companyID='{$this->pg['companyID']}';
                ";
                if($data['tariff']>0){
                    $balance = $balance - $data['tariff'];
                    $sql .="
                UPDATE [globalAuth].[dbo].[company]
                SET [balance]='$balance',
                [tariff]=(
                        SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
                        WHERE companyID = '{$this->pg['companyID']}'
                        )
                WHERE id='{$this->pg['companyID']}';
                INSERT INTO [globalAuth].[dbo].[billingLog]
                ([type],[companyID],[sum],[userID])VALUES(
                '4',
                '{$this->pg['companyID']}',
                '{$data['tariff']}',
                '{$_SESSION['auth']['info']['id']}'
                );
                    INSERT INTO [globalAuth].[dbo].[billingLog]
                ([type],[companyID],[sum],[userID])VALUES(
                '3',
                '{$this->pg['companyID']}',
                (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'),
                '{$_SESSION['auth']['info']['id']}'
                );
                    ";
                }

        }

        $res = qry::queryExec($sql);
        if($res)auth::debit();

        return array(
            'set' => $res ? 1 : 0,
            'project' => $this->getAccess(),
            'company' => company::getCompany(),
        );
    }

    //Удалить услугу у компании
    public function deleteAccess(){
        auth::giveAccess(2);
        $data = json_decode($this->pg['data'],true);
        $sql = "
        DELETE [globalAuth].[dbo].[access] WHERE id='{$data['id']}';
        UPDATE [globalAuth].[dbo].[users] SET appDefault=NULL WHERE appDefault = (SELECT [name] FROM [globalAuth].[dbo].[project] WHERE id={$data['idProject']});
        UPDATE [globalAuth].[dbo].[users] SET hash=NULL WHERE companyID={$this->pg['companyID']};
        ";

        $except = [
            'wialon'=>'companyAuto',
            'SKR5.2'=>'companySKR',
        ];
        if(array_key_exists($data['name'],$except)){
            $sql .= "DELETE [globalAuth].[dbo].[{$except[$data['name']]}] WHERE companyID={$this->pg['companyID']};";
        }

        if($data['tariff']>0){
            $sql.="
            DECLARE @tariff NUMERIC(18,2);
            SET @tariff = (
                    SELECT ISNULL(SUM([tariff]),0) FROM [globalAuth].[dbo].[access]
                    WHERE companyID = '{$this->pg['companyID']}'
            );
            UPDATE [globalAuth].[dbo].[company] SET [tariff]=@tariff WHERE id='{$this->pg['companyID']}';
            INSERT INTO [globalAuth].[dbo].[billingLog]
                ([type],[companyID],[sum],[userID])VALUES(
                '3',
                '{$this->pg['companyID']}',
                 @tariff,
                '{$_SESSION['auth']['info']['id']}'
                );
            ";
        }

        $res = qry::queryExec($sql);
        if($res)auth::debit();

        return array(
            'set'=>$res,
            'company' => company::getCompany(),
        );
    }

    //Получить список услуг компании
    static function getAccess(){
        auth::giveAccess(1.5);
        global $pg;
        $sql = "
       SELECT
       access.id
      ,access.[idProject]
      ,access.[tariff]
      ,access.[settings]
      ,project.[name]
      ,project.[title]
       FROM [globalAuth].[dbo].[access]
       INNER JOIN [globalAuth].[dbo].[project] ON project.id = access.idProject
       WHERE access.companyID = {$pg['companyID']}
       ORDER BY access.id DESC
        ";
        return qry::queryGet($sql);
    }

    //Получить кол-во едениц исключений
    public function getSumExcept(){
        $except = [
            'inMarket'=>'sites',
            'inMobile'=>'companyApp',
        ];
        $sql = "SELECT COUNT(1) as [count] FROM [globalAuth].[dbo].[{$except[$this->pg['project']]}] WHERE [companyID] = '{$this->pg['companyID']}';";
        return ['sum'=>qry::queryGet($sql)[0]['count']];
    }
}