<?php
class lock
{
    private $pg;

    function __construct()
    {
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if (method_exists($this, $method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        } else header("HTTP/1.1 404 Not Found");
    }
    //получение баланса компании
    public function getBalance(){
        return company::getBalance();
    }
}