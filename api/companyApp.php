<?php
class companyApp {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Получить приложения привязанные компании
    public function get(){
        auth::giveAccess(1.5);
        $sql = "
        SELECT
       [id]
      ,[name]
      ,[description]
      ,[icon]
      ,[ver]
        FROM [globalAuth].[dbo].[companyApp]
        WHERE [companyID] = '{$this->pg['companyID']}'
        ORDER BY id DESC
        ;";
        return qry::queryGet($sql);
    }

    //Удалить приложение компании
    public function delete(){
        auth::giveAccess(1.5);
        $sql = "
        DELETE [globalAuth].[dbo].[companyApp] WHERE id={$this->pg['id']};
        UPDATE [globalAuth].[dbo].[access]
        SET [tariff] = CAST(JSON_VALUE([settings], '$.tariff') as NUMERIC(18,2))*(
             SELECT COUNT(1) FROM [globalAuth].[dbo].[companyApp]
             WHERE [companyID]='{$this->pg['companyID']}'
        )
        WHERE companyID = '{$this->pg['companyID']}'
        AND idProject = (SELECT id FROM [globalAuth].[dbo].[project] WHERE [name] = 'inMobile');
        UPDATE [globalAuth].[dbo].[company]
        SET [tariff]=(
             SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
             WHERE companyID = '{$this->pg['companyID']}'
        )
        WHERE id='{$this->pg['companyID']}';
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '3',
        '{$this->pg['companyID']}',
        (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'),
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
        $file = "mobile-app/app_{$this->pg['id']}.zip";
        if (file_exists($file)) unlink($file);
        return array(
            'set'=>qry::queryExec($sql),
            'data'=>$this->get(),
            'company' => company::getCompany(),
            'access' => access::getAccess(),
        );
    }

    //Добавить/изменить приложение компании
    public function set(){
        auth::giveAccess(1.5);
        $data = json_decode($this->pg['data'],true);
        if(qry::queryExist("SELECT 1 FROM [globalAuth].[dbo].[companyApp] WHERE [name]='{$data['name']}' AND [companyID]='{$this->pg['companyID']}' AND id!='{$data['id']}'"))return ['error'=>'Такое имя уже существует!'];
        $fields = [
            'name',
            'description',
            'icon',
            'ver',
        ];
        $res = false;
        if($data['id']){
            $set = "";
            foreach ($fields as $field) {
                if ($set) $set .= ",";
                $set .= "[$field]=";
                $set .= $data[$field] == "" ? "NULL" : "'{$data[$field]}'";
            }
            $sql = "UPDATE [globalAuth].[dbo].[companyApp] SET $set WHERE id={$data['id']};";
            $res = qry::queryExec($sql);
        }else{
            $project = qry::queryGet("
                  SELECT
                  id
                  ,JSON_VALUE([settings], '$.tariff') as tariff
                  FROM [globalAuth].[dbo].[access]
                  WHERE companyID = '{$this->pg['companyID']}'
                  AND idProject = (SELECT id FROM [globalAuth].[dbo].[project] WHERE [name] = 'inMobile');
            ")[0];
            if($project['tariff']>0){
                $balance = qry::queryGet("SELECT balance FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'")[0]['balance'];
                if($balance < $project['tariff'])return ['error'=>'Не достаточно средств!'];
            }
            $key = "";
            $values = "";
            foreach ($fields as $field) {
                if ($key) $key .= ",";
                $key .= "[$field]";
                if ($values) $values .= ",";
                $values .= $data[$field] == "" ? "NULL" : "'{$data[$field]}'";
            }
            $sql = "INSERT INTO [globalAuth].[dbo].[companyApp]($key,[companyID])VALUES($values,'{$this->pg['companyID']}');";
            $data['id'] = qry::queryExec($sql,1,true);
            $res = !!$data['id'];
            if ($res && $project['tariff'] > 0) {
                $sql = "
        UPDATE [globalAuth].[dbo].[access]
        SET [tariff] = CAST(JSON_VALUE([settings], '$.tariff') as NUMERIC(18,2))*(
             SELECT COUNT(1) FROM [globalAuth].[dbo].[companyApp]
             WHERE [companyID]='{$this->pg['companyID']}'
        )
        WHERE id = '{$project['id']}';
        UPDATE [globalAuth].[dbo].[company]
        SET [balance]=[balance]-{$project['tariff']},
        [tariff]=(
             SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
             WHERE companyID = '{$this->pg['companyID']}'
        )
        WHERE id='{$this->pg['companyID']}';
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '4',
        '{$this->pg['companyID']}',
        '{$project['tariff']}',
        '{$_SESSION['auth']['info']['id']}'
        );
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '3',
        '{$this->pg['companyID']}',
        (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'),
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
                $res = qry::queryExec($sql);
            }
        }
        if($res && !empty($_FILES)){
            $dir = "mobile-app";
            $file = "$dir/app_{$data['id']}.zip";
            if (!file_exists($dir)) mkdir($dir, null, true);
            $res = move_uploaded_file($_FILES['file']['tmp_name'], $file);
        }
        return array(
            'set'=>$res,
            'data'=>$this->get(),
            'company' => company::getCompany(),
            'access' => access::getAccess(),
        );
    }

    //Получить приложения привязанные компании текущего пользователя
    public function getApp(){
        $sql = "
        SELECT
       [id]
      ,[name]
      ,[description]
      ,[icon]
      ,[ver]
        FROM [globalAuth].[dbo].[companyApp]
        WHERE [companyID] = '{$_SESSION['auth']['info']['companyID']}'
        ORDER BY id DESC
        ;";
        return qry::queryGet($sql);
    }
}