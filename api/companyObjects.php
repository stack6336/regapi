<?php
class companyObjects {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Получить список объектов компании
    public function getCompanyObjects(){
        auth::giveAccess(2);
        return qry::queryGet("SELECT [objectID] FROM [globalAuth].[dbo].[companyObjects] WHERE [companyID]='{$this->pg['companyID']}';");
    }

    //Прикрепить объекты к компании
    public function setCompanyObjects(){
        auth::giveAccess(2);
        $data = json_decode($this->pg['data'],true);
        $objects = qry::queryGet("SELECT [objectID] FROM [globalAuth].[dbo].[companyObjects] WHERE [companyID]='{$this->pg['companyID']}'");
        $sql = "DELETE [globalAuth].[dbo].[companyObjects] WHERE [companyID]='{$this->pg['companyID']}';";
        if(count($data)>0){
            $sql .= "INSERT INTO [globalAuth].[dbo].[companyObjects] ([companyID],[objectID]) VALUES ";
            foreach($data as $k=>$val){
                if($k>0) $sql .= ",";
                $sql .= "('{$this->pg['companyID']}','{$val['id']}')";
            }
            $sql .= ";";
        }
        $delete = [];
        foreach ($objects as $val){
            $find = false;
            foreach ($data as $v){if($v['id']==$val['objectID'])$find=true;}
            if(!$find)array_push($delete,$val['objectID']);
        }
        if(count($delete)>0) {
            $delete = implode(",", $delete);
            $sql .= "
        DELETE [globalAuth].[dbo].[userObjects]
        WHERE [userID] IN (SELECT [id] FROM [globalAuth].[dbo].[users] WHERE [companyID] = '{$this->pg['companyID']}')
        AND [objectID] IN ($delete);
        ";
        }
        return ['set'=>qry::queryExec($sql)];
    }

    //Получить список компаний объекта
    public function getObjectCompany(){
        auth::giveAccess(2);
        return qry::queryGet("SELECT [companyID] FROM [globalAuth].[dbo].[companyObjects] WHERE [objectID]='{$this->pg['objectID']}';");
    }

    //Прикрепить компании к объекту
    public function setObjectCompany(){
        auth::giveAccess(2);
        $data = json_decode($this->pg['data'],true);
        $company = qry::queryGet("SELECT [companyID] FROM [globalAuth].[dbo].[companyObjects] WHERE [objectID]='{$this->pg['objectID']}'");
        $sql = "DELETE [globalAuth].[dbo].[companyObjects] WHERE [objectID]='{$this->pg['objectID']}';";
        if(count($data)>0){
            $sql .= "INSERT INTO [globalAuth].[dbo].[companyObjects] ([companyID],[objectID]) VALUES ";
            foreach($data as $k=>$val){
                if($k>0) $sql .= ",";
                $sql .= "('{$val['id']}','{$this->pg['objectID']}')";
            }
            $sql .= ";";
        }
        $delete = [];
        foreach ($company as $val){
            $find = false;
            foreach ($data as $v){if($v['id']==$val['companyID'])$find=true;}
            if(!$find)array_push($delete,$val['companyID']);
        }
        if(count($delete)>0) {
            $delete = implode(",", $delete);
            $sql .= "
            DELETE [globalAuth].[dbo].[userObjects]
            WHERE [userID] IN (SELECT [id] FROM [globalAuth].[dbo].[users] WHERE [companyID] IN ($delete))
            AND [objectID] = '{$this->pg['objectID']}';
            ";
        }
        return ['set'=>qry::queryExec($sql)];
    }
}