<?php
class device {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Получить лог устройств которые устанавливали мобильное приложение
    public function getDevice(){
        auth::giveAccess(1.2);
        $where = "";
        if($_SESSION['auth']['info']['access']<=1.5 && $_SESSION['auth']['info']['companyHead']=='1'){
            $where = "WHERE t3.[INN]='{$_SESSION['auth']['info']['INN']}'";
        }else if($_SESSION['auth']['info']['access']<=1.5){
            $where = "WHERE t3.[id]='{$_SESSION['auth']['info']['companyID']}'";
        }
        $time = time();
        $sql = "
        SELECT
       t1.[id]
      ,t1.[IMEI]
      ,t1.[dateTime]
      ,t1.[timeZone]
      ,t1.[dateTimeS]
      ,t1.[lat]
      ,t1.[long]
      ,t1.[app]
      ,t1.[userID]
	  ,t2.[surName]
      ,t2.[firstName]
      ,t2.[lastName]
      ,t3.[name] as company
      ,t3.[INN]
      ,t3.[KPP]
      ,t1.[verWeb]
      ,CASE
      WHEN t1.[online] IS NULL OR ($time-t1.[online])>=900 THEN 0 ELSE 1
      END as [online]
FROM [globalAuth].[dbo].[phoneList] t1
LEFT JOIN [globalAuth].[dbo].[users] t2 ON t2.id=t1.userID
LEFT JOIN [globalAuth].[dbo].[company] t3 ON t3.id=t2.companyID
$where
ORDER BY t1.[dateTimeS] DESC
        ";
        return qry::queryGet($sql);
    }

    //Запланировать команду на обновления устройства
    public function updDevice(){
        return ['success'=>qry::queryExec("UPDATE [globalAuth].[dbo].[phoneList] SET [update]=1 WHERE [IMEI] = '{$this->pg['IMEI']}'")];
    }

}