<?php
class companySKR {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    //Получить ключи привязанные компании
    public function get(){
        auth::giveAccess(1.5);
        $sql = "
        SELECT
       [id]
      ,[key]
      ,[description]
        FROM [globalAuth].[dbo].[companySKR]
        WHERE [companyID] = '{$this->pg['companyID']}'
        ORDER BY id DESC
        ;";
        return qry::queryGet($sql);
    }

    //Получить ключи привязанные компании текущего пользователя
    public function getSKR(){
        $where = "";
        if($_SESSION['auth']['info']['access']<2) {
            if ($_SESSION['auth']['info']['companyHead']=='1') {
                $where = "WHERE [companyID] IN (SELECT id FROM [globalAuth].[dbo].[company] WHERE [INN]='{$_SESSION['auth']['info']['INN']}' AND [lock]=0)";
            } else {
                $where = "WHERE [companyID] = '{$_SESSION['auth']['info']['companyID']}'";
            }
        }
        $date = time();
        $sql = "
        SELECT
		[key]
		,CASE WHEN [description] IS NULL THEN [key] ELSE [description] END as [description]
		,[companyID]
		,CASE WHEN [online] IS NOT NULL AND (($date - [online]) / 60)<15 THEN 1 ELSE 0 END as [online]
        FROM [globalAuth].[dbo].[companySKR]
        $where
        ORDER BY [description]
        ;";
        return qry::queryGet($sql);
    }

    //Удалить ключ компании
    public function delete(){
        auth::giveAccess(1.5);
        $sql = "
        DELETE [globalAuth].[dbo].[companySKR] WHERE id={$this->pg['id']};
        UPDATE [globalAuth].[dbo].[access]
        SET [tariff] = CAST(JSON_VALUE([settings], '$.tariff') as NUMERIC(18,2))*(
             SELECT COUNT(1) FROM [globalAuth].[dbo].[companySKR]
             WHERE [companyID]='{$this->pg['companyID']}'
        )
        WHERE companyID = '{$this->pg['companyID']}'
        AND idProject = (SELECT id FROM [globalAuth].[dbo].[project] WHERE [name] = 'SKR5.2');
        UPDATE [globalAuth].[dbo].[company]
        SET [tariff]=(
             SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
             WHERE companyID = '{$this->pg['companyID']}'
        )
        WHERE id='{$this->pg['companyID']}';
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '3',
        '{$this->pg['companyID']}',
        (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'),
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
        return array(
            'set'=>qry::queryExec($sql),
            'data'=>$this->get(),
            'company' => company::getCompany(),
            'access' => access::getAccess(),
        );
    }

    //Добавить/изменить ключ компании
    public function set(){
        auth::giveAccess(1.5);
        $data = json_decode($this->pg['data'],true);
        if(qry::queryExist("SELECT 1 FROM [globalAuth].[dbo].[companySKR] WHERE [key]='{$data['key']}' AND [companyID]='{$this->pg['companyID']}' AND id!='{$data['id']}'"))return ['error'=>'Такой ключ уже привязан к компании!'];
        $fields = [
            'key',
            'description',
        ];
        if($data['id']){
            $set = "";
            foreach ($fields as $field) {
                if ($set) $set .= ",";
                $set .= "[$field]=";
                $set .= $data[$field] == "" ? "NULL" : "'{$data[$field]}'";
            }
            $sql = "UPDATE [globalAuth].[dbo].[companySKR] SET $set WHERE id={$data['id']};";
        }else {
            $project = qry::queryGet("
                  SELECT
                  id
                  ,JSON_VALUE([settings], '$.tariff') as tariff
                  FROM [globalAuth].[dbo].[access]
                  WHERE companyID = '{$this->pg['companyID']}'
                  AND idProject = (SELECT id FROM [globalAuth].[dbo].[project] WHERE [name] = 'SKR5.2');
            ")[0];
            if($project['tariff']>0){
                $balance = qry::queryGet("SELECT balance FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'")[0]['balance'];
                if($balance < $project['tariff'])return ['error'=>'Не достаточно средств!'];
            }
            $key = "";
            $values = "";
            foreach ($fields as $field) {
                if ($key) $key .= ",";
                $key .= "[$field]";
                if ($values) $values .= ",";
                $values .= $data[$field] == "" ? "NULL" : "'{$data[$field]}'";
            }
            $sql = "INSERT INTO [globalAuth].[dbo].[companySKR]($key,[companyID])VALUES($values,'{$this->pg['companyID']}');";
            if ($project['tariff'] > 0) {
                $sql .= "
        UPDATE [globalAuth].[dbo].[access]
        SET [tariff] = CAST(JSON_VALUE([settings], '$.tariff') as NUMERIC(18,2))*(
             SELECT COUNT(1) FROM [globalAuth].[dbo].[companySKR]
             WHERE [companyID]='{$this->pg['companyID']}'
        )
        WHERE id = '{$project['id']}';
        UPDATE [globalAuth].[dbo].[company]
        SET [balance]=[balance]-{$project['tariff']},
        [tariff]=(
             SELECT SUM([tariff]) FROM [globalAuth].[dbo].[access]
             WHERE companyID = '{$this->pg['companyID']}'
        )
        WHERE id='{$this->pg['companyID']}';
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '4',
        '{$this->pg['companyID']}',
        '{$project['tariff']}',
        '{$_SESSION['auth']['info']['id']}'
        );
        INSERT INTO [globalAuth].[dbo].[billingLog]
        ([type],[companyID],[sum],[userID])VALUES(
        '3',
        '{$this->pg['companyID']}',
        (SELECT tariff FROM [globalAuth].[dbo].[company] WHERE id='{$this->pg['companyID']}'),
        '{$_SESSION['auth']['info']['id']}'
        );
        ";
            }
        }
        return array(
            'set'=>qry::queryExec($sql),
            'data'=>$this->get(),
            'company' => company::getCompany(),
            'access' => access::getAccess(),
        );
    }

    //Верификация ключа относительно компании
    public function verifyKey(){
        return ['verify'=>qry::queryExist("SELECT 1 FROM [globalAuth].[dbo].[companySKR] WHERE [key]='{$this->pg['key']}' AND companyID='{$_SESSION['auth']['info']['companyID']}'")];
    }

    //Запись статуса online СКР
    public function setOnline(){
        if($this->pg['key']) {
            $date = time();
            $sql = "UPDATE [globalAuth].[dbo].[companySKR] SET [online]=$date WHERE [key]='{$this->pg['key']}';";
            $res = qry::queryExec($sql);
        }else $res = false;
        return ['success'=>$res];
    }
}