<?php

header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: origin, x-requested-with, content-type, cache-control, if-modified-since");
header("Access-Control-Allow-Methods: HEAD, GET, POST, OPTIONS");
if('OPTIONS' == $_SERVER['REQUEST_METHOD'])status_header(200);


$GLOBALS['PARAM'] = parse_ini_file('.env');
//foreach($_POST as $k=>$v){$_POST[$k]=str_replace("'","''",$_POST[$k]);}
//foreach($_GET as $k=>$v){$_GET[$k]=str_replace("'","''",$_GET[$k]);}
function reqFile($dir){
    foreach (scandir($dir) as $value) {if ($value != '.' && $value != '..') {
        if(is_dir("$dir/$value")) reqFile("$dir/$value");
        else require_once("$dir/$value");
    }}
}
reqFile('lib');
reqFile('api');
$devKey = "7cafffc3c755da4f54441001ba918ff47ea142cb";
$saltPass = "Qq123456";
$salt = '$6$rounds=5000$'.$saltPass.'$';
$pg = qry::rout();
$class = $pg['rout'];
if (
    $class != "auth"
    && $pg['hash'] != $devKey
    && (
        !auth::testAuth()['auth']
        || (
            $_SESSION['auth']['info']['access']<3
            && $_SESSION['auth']['info']['lock']=="1"
            && $class != "lock"
        )
    )
) header("HTTP/1.1 403 Forbidden");
else {
    if (class_exists($class)) new $class();
    else header("HTTP/1.1 404 Not Found");
}
